﻿using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KIMS.AdminUserControls
{
    /// <summary>
    /// Interaction logic for AdminLogin.xaml
    /// </summary>
    public partial class AdminLogin : UserControl
    {
        public AdminLogin()
        {
            InitializeComponent();
        }
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseAdminSetting;
        public event EventHandler EventLoginAdmin;
        public event EventHandler EventWarningAlert;
        string processName;
        Process proc;
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            txtName.Focus();
            Logger.Info("Completed");
        }

        private void btnLogin_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            ValidateLogin();
            Logger.Info("Completed");
        }

        private void ValidateLogin()
        {
            Logger.Info("Initiated");
            CloseTabTip();
            if (!string.IsNullOrEmpty(txtName.Text) && !string.IsNullOrEmpty(txtPassword.Password))
            {
                if (Apps.PingCheck())
                {
                    if (Apps.ValidateAdmin(txtName.Text, txtPassword.Password))
                    {
                        SaveCredentials(txtName.Text.Trim(), txtPassword.Password.Trim());
                        EventLoginAdmin(this, null);
                    }
                    else
                    {
                        EventWarningAlert("Invalid username and password.", null);
                        ClearFields();
                    }
                }
                else if (txtName.Text.Trim().Equals(Utilities.UserName) && txtPassword.Password.Trim().Equals(Utilities.Password))
                {
                    EventLoginAdmin(this, null);
                }
                else
                {
                    ClearFields();
                    EventWarningAlert("Invalid username and password.", null);
                }
            }
            else
            {
                ClearFields();
                EventWarningAlert("Username and Password should not be empty.", null);
            }
            Logger.Info("Completed");
        }

        private void ClearFields()
        {
            Logger.Info("Initiated");
            txtName.Text = string.Empty;
            txtPassword.Password = string.Empty;
            txtName.Focus();
            Logger.Info("Completed");
        }

        private void SaveCredentials(string userName, string password)
        {
            Logger.Info("Initiated");
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                config.AppSettings.Settings["Locationusername"].Value = userName;
                config.AppSettings.Settings["Locationpassword"].Value = password;


                RegistryKey regKey = Registry.CurrentUser.OpenSubKey("Software\\TouchOnCloud\\CMC");
                if (regKey != null)
                {
                    config.AppSettings.Settings["Username"].Value = Convert.ToString(regKey.GetValue("UserName"));
                    config.AppSettings.Settings["Password"].Value = Convert.ToString(regKey.GetValue("Password"));
                }
                else
                {
                    Logger.Info("CMC Username and Password are empty, it may interrupt HomeSlideShow and Offer Images.");
                }
                config.Save(ConfigurationSaveMode.Full, true);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            Logger.Info("Completed");
        }

        private void txtName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                ValidateLogin();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter))
            {
                ValidateLogin();
            }
        }
        
        private void OpenVirtualKeyboard()
        {
            try
            {
                CloseOSK();
                proc = Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\tabtip.exe");
                processName = proc.ProcessName;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        private void CloseOSK()
        {
            Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
            foreach (Process proc in process)
            {
                proc.CloseMainWindow();    //kill the process
            }
        }
        private void CloseTabTip()
        {
            try
            {
                Process[] process = Process.GetProcessesByName(processName);   //gets all the process with name processName
                foreach (Process proc in process)
                {
                    proc.Kill();    //kill the process before closing usercontrol
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        
        private void txtName_TouchDown(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void txtPassword_TouchDown(object sender, TouchEventArgs e)
        {
            OpenVirtualKeyboard();
        }

        private void btnCanelLogin_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            CloseTabTip();
            EventCloseAdminSetting(this, null);
            Logger.Info("Completed");
        }
    }
}
