﻿using ApolloDB.Models;
using KIMS.UserControls.AlertControls;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KIMS.AdminUserControls
{
    /// <summary>
    /// Interaction logic for QuestionsUC.xaml
    /// </summary>
    public partial class QuestionsUC : UserControl
    {
        #region variables
        Process proc;
        private string processName;
        public event EventHandler EventCloseQuestionUC;
        private Logger Logger = LogManager.GetCurrentClassLogger();        
        #endregion

        public QuestionsUC()
        {
            InitializeComponent();
        }
        private void txtQuestion_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }

        private void btnSave_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (string.IsNullOrEmpty(txtQuestion.Text))
            {
                MessageBox.Show("Please Enter Question.", "KIMS");
            }
            else
            {
                try
                {
                    FeedBackQuestions feedBackQuestions = new FeedBackQuestions();
                    long maxID = Apps.myDbContext.FEEDBACKQUESTIONS.ToList().Count > 0
                        ? (Apps.myDbContext.FEEDBACKQUESTIONS.Max(c => c.ID) + 1)
                        : 1;
                    feedBackQuestions.ID = maxID;
                    feedBackQuestions.LASTUPDATEDTIME = DateTime.Now;
                    feedBackQuestions.QUESTION = txtQuestion.Text.ToUpper();
                    Apps.myDbContext.FEEDBACKQUESTIONS.Add(feedBackQuestions);
                    Apps.myDbContext.SaveChanges();
                    Apps.KillTabTip();
                    //SuccessAlertUC sAlert = new SuccessAlertUC
                    //{
                    //    txtSuccessMessage = { Text = "Question added successfully !!!" }
                    //};
                    //MainLayout.Children.Add(sAlert);
                    //sAlert.EventCloseSuccessAlert += sAlert_EventCloseSuccessAlert;
                    txtQuestion.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }
            }
            Logger.Info("Completed");
        }

        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Apps.KillTabTip();            
            EventCloseQuestionUC(this, null);
            Logger.Info("Completed");
        }
    }
}
