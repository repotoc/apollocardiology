﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KIMS.AdminUserControls
{
    /// <summary>
    /// Interaction logic for SlideShowIdleTimer.xaml
    /// </summary>
    public partial class SlideShowIdleTimer : UserControl
    {
        public SlideShowIdleTimer()
        {
            InitializeComponent();
        }
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseIdleTimer;
        public event EventHandler EventSuccessAlert;
        public event EventHandler EventWarningAlert;
        private Configuration config;
        private int idleTimerSeconds; 
        #endregion
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Apps.KillTabTip();
            EventCloseIdleTimer(this, null);
            Logger.Info("Completed");   
        }
        private void btnSave_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            int idleSeconds = 0;
            if (config == null)
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            }

            if (Int32.TryParse(txtTimer.Text.Trim(), out idleSeconds))
            {
                if (idleSeconds >= 120)
                {
                    config.AppSettings.Settings["IdleTimer"].Value = idleSeconds.ToString();
                    config.Save(ConfigurationSaveMode.Full, true);
                    EventSuccessAlert("Details Saved Successfully !!!", null);
                    btnBack_PreviewTouchUp(sender, e);
                }
                else
                {
                    EventWarningAlert("Idle seconds must be greater than 120 seconds", null);
                    //MessageBox.Show("Idle seconds must be greater than 120 seconds.", "DRL", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            Logger.Info("Completed");
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            if (!(Int32.TryParse(config.AppSettings.Settings["IdleTimer"].Value, out idleTimerSeconds)) && idleTimerSeconds <= 120)
            {
                idleTimerSeconds = 120;
            }
            txtTimer.Text = idleTimerSeconds.ToString();
            Logger.Info("Completed");
        }
        private void txtTimer_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillTabTip();
        }

        private void txtTimer_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
            txtTimer.Focus();
        }
    }
}
