﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KIMS.AdminUserControls
{
    /// <summary>
    /// Interaction logic for AdminSettings.xaml
    /// </summary>
    public partial class AdminSettings : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventLogoutSettings;
        public event EventHandler EventQuestions;
        public event EventHandler EventReports;
        public event EventHandler EventSlideShowIdleTimer;
        public event EventHandler EventOpenSetAutoSync;
        public event EventHandler EventEmailConfig;
        #endregion
        public AdminSettings()
        {
            InitializeComponent();
        }
        private void btnLogout_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventLogoutSettings(this, null);

            Logger.Info("Completed");
        }
        private void btnSetAutoSync_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventOpenSetAutoSync(this, null);
            Logger.Info("Completed");
        }
        private void btnShowDesktop_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            System.Diagnostics.Process.Start("explorer");
            Logger.Info("Completed");
        }

        private void btnRestart_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (MessageBox.Show("Are you sure want to restart the system.", "DRL", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("shutdown", "/r /f /t 0");
            }
            Logger.Info("Completed");
        }

        private void btnShutdown_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (MessageBox.Show("Are you sure want to shutdown the system.", "DRL", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("shutdown", "/s /f /t 0");
            }
            Logger.Info("Completed");
        }
        private void BtnQuestions_OnPreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventQuestions(this, null);
            Logger.Info("Completed");
        }
        private void BtnApplicationIdle_OnPreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventSlideShowIdleTimer(this, null);
            Logger.Info("Completed");
        }
        private void BtnFeedBackReports_OnPreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventReports(this, null);
            Logger.Info("Completed");
        }

        private void btnEmailConfig_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventEmailConfig(this, null);
            Logger.Info("Completed");
        }
    }
}
