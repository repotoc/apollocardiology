﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace KIMS.AdminUserControls
{
    /// <summary>
    /// Interaction logic for ReportsUC.xaml
    /// </summary>
    public partial class ReportsUC : UserControl
    {
        public ReportsUC()
        {
            InitializeComponent();
        }
        public event EventHandler EventCloseReportUC;
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EventCloseReportUC(this, null);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //Apps.dbContext = new AINUDB.DBFolder.AINUDBContext();

            //if (Apps.dbContext.FEEDBACKSUBMITS.ToList().Count > 0)
            //{
            //    dt = new DataTable();

            //    //create datatable columns list
            //    var Columns = new[] {
            //                        new DataColumn("EMPLOYEEID", typeof(string)),
            //                        new DataColumn("EMPLOYEENAME", typeof(string)),   
            //                        new DataColumn("QUESTION", typeof(string)),
            //                        new DataColumn("RATING", typeof(string)),
            //                            };
            //    dt.Columns.AddRange(Columns);         //Add the columns list to datatable
            //    lock (lockTarget)
            //    {
            //        foreach (var item in Apps.dbContext.FEEDBACKSUBMITS.ToList().OrderByDescending(c => c.LASTUPDATEDTIME))
            //        {
            //            DataRow dtRow = dt.NewRow();
            //            dtRow["EMPLOYEEID"] = item.PATIENTID;
            //            dtRow["EMPLOYEENAME"] = item.PATIENTNAME;
            //            dtRow["QUESTION"] = item.FEEDBACKQUEST.QUESTION;
            //            dtRow["RATING"] = item.Rating;
            //            dt.Rows.Add(dtRow);
            //        }
            //        dgFeedbackReports.ItemsSource = dt.DefaultView;
            //    }
            //}
        }

        private void svReports_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
