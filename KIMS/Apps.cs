﻿using ApolloDB.DBFolder;
using KIMS.UserControls;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace KIMS
{
    public static class Apps
    {
        static Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
        public static ApolloDBContext myDbContext = new ApolloDBContext();
        public static double MaxIdleSeconds = 25;
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        public static string strPath = AppDomain.CurrentDomain.BaseDirectory;

        public static void AssignMaxIdleTimer()
        {
            int seconds = 0;
            Int32.TryParse(config.AppSettings.Settings["MaxApplicationIdleTimer"].Value, out seconds);
            if (seconds > 25)
            {
                Apps.MaxIdleSeconds = seconds;
            }
        }

        public static int FetchMaxIdleTimer()
        {
            int seconds = 0;
            Int32.TryParse(config.AppSettings.Settings["MaxApplicationIdleTimer"].Value, out seconds);
            return seconds;
        }

        public static void KillTabTip()
        {
            try
            {
                if (Process.GetProcessesByName("TabTip").Count() > 0)
                {
                    Process fetchProcess = Process.GetProcessesByName("TabTip").FirstOrDefault();
                    if (fetchProcess != null)
                    {
                        fetchProcess.Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        public static void OpenVirtualKeyboard()
        {
            try
            {
                KillTabTip();
                Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }


        public static void PingTest(bool raiseWindow = true)
        {
            try
            {
                using (Ping ping = new Ping())
                {
                    PingReply pingStatus = ping.Send("www.google.com");

                    if (pingStatus.Status == IPStatus.Success)
                    {

                    }
                    else
                    {
                        if (raiseWindow)
                        {
                            InternetDown downDlg = new InternetDown();
                            downDlg.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                if (raiseWindow)
                {
                    InternetDown downDlg = new InternetDown();
                    downDlg.ShowDialog();
                    downDlg.EventCloseConnection += downDlg_EventCloseConnection;
                }
            }
        }

        static void downDlg_EventCloseConnection(object sender, EventArgs e)
        {
            InternetDown downDlg = (InternetDown)sender;
            downDlg.Close();
        }

        public static bool PingCheck()
        {
            try
            {
                using (Ping ping = new Ping())
                {

                    PingReply pingStatus = ping.Send("www.google.com");

                    if (pingStatus.Status == IPStatus.Success)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                return false;
            }
        }

        public static bool ValidateAdmin(string userName, string password)
        {
            bool isValid = false;
            try
            {
                string serverPath = Utilities.SlideShowServerUrl;
                string cmcLoginPath = Utilities.CmcLoginService;
                string frameURI = serverPath + "/" + cmcLoginPath + "/";
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    Uri baseAddress = new Uri(frameURI);
                    client.BaseAddress = baseAddress;
                    if (!client.DefaultRequestHeaders.Contains("Authorization"))
                    {
                        byte[] authBytes = Encoding.UTF8.GetBytes((userName + ":" + password).ToCharArray());
                        client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(authBytes));
                    }
                    //string address = "api/Admin/ValidateAdmin?userName=" + userName + "&password=" + password;
                    string address = "api/Login?userName=" + userName + "&password=" + password + "&loginType=S";
                    HttpResponseMessage response = client.GetAsync(address).Result;
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        isValid = response.Content.ReadAsAsync<bool>().Result;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            return isValid;
        }

    }
}
