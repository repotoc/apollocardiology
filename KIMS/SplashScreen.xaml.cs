﻿using KIMS.FeedbackQuestLogic;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KIMS
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        #region variables
        Logger Logger = LogManager.GetCurrentClassLogger();
        BackgroundWorker bgw = null; 
        #endregion

        public SplashScreen()
        {
            InitializeComponent();
        }
        private void SplashScreen_OnContentRendered(object sender, EventArgs e)
        {
            Logger.Info("Initiated");

            using (bgw = new BackgroundWorker { WorkerReportsProgress = true, WorkerSupportsCancellation = true })
            {
                bgw.DoWork += bgw_DoWork;
                bgw.ProgressChanged += bgw_ProgressChanged;
                bgw.RunWorkerCompleted += bgw_RunWorkerCompleted;
                bgw.RunWorkerAsync();
            }
            Logger.Info("Completed");
        }

        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Logger.Info("loading completed");
            try
            {
                bgw = null;
                this.Hide();
                MainWindow HomePage = new MainWindow();
                HomePage.ShowDialog();
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            Logger.Info("Completed");
        }
        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Logger.Info("Initiated");
            lblLoadingText.Text = e.UserState as string;
            Logger.Info("Completed");
        }
        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            bgw.ReportProgress(1, "Loading Database...");
            Apps.myDbContext = new ApolloDB.DBFolder.ApolloDBContext();
            Apps.myDbContext.CATEGORY.ToList();
            bgw.ReportProgress(2, "Intiating Feedback Questions...");
            FeedBackQuestionInitScript feedbackScript = new FeedBackQuestionInitScript();
            feedbackScript.RunInitScript();
        }
    }
}
