﻿using ApolloDB.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KIMS.UserControls.DoctorControls
{
    /// <summary>
    /// Interaction logic for DoctorProfileView.xaml
    /// </summary>
    public partial class DoctorProfileView : UserControl
    {
        public DoctorProfileView()
        {
            InitializeComponent();
        }
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public Doctor docItem { get; set; }
        public event EventHandler EventCloseDocView;
        private TranslateTransform transform = new TranslateTransform();
        TouchPoint anchorPoint;
        TouchPoint currentPoint;
        bool isInDrag = false;
        int id = 0;

        #endregion

        private void btnCloseDrView_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseDocView(this, null);
        }

        private void UserControl_TouchMove(object sender, TouchEventArgs e)
        {
            if (isInDrag && id.Equals(e.TouchDevice.Id))
            {
                //var element = sender as FrameworkElement;
                currentPoint = e.GetTouchPoint(null);
                transform.X += currentPoint.Position.X - anchorPoint.Position.X;
                transform.Y += (currentPoint.Position.Y - anchorPoint.Position.Y);
                this.RenderTransform = transform;
                anchorPoint = currentPoint;

                FrameworkElement felement = e.Source as FrameworkElement;
                Panel pnl = felement.Parent as Panel;

                for (int i = 0; i < pnl.Children.Count; i++)
                {
                    Panel.SetZIndex(pnl.Children[i],
                        pnl.Children[i] == felement ? pnl.Children.Count : i);
                }
            }
        }

        private void UserControl_TouchEnter(object sender, TouchEventArgs e)
        {
            if (id == 0)
            {
                id = e.TouchDevice.Id;
            }
            if (id.Equals(e.TouchDevice.Id))
            {
                var element = sender as FrameworkElement;
                anchorPoint = e.GetTouchPoint(null);
                element.CaptureMouse();
                isInDrag = true;
                e.Handled = true;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            this.DataContext = docItem;
            Logger.Info("Completed");
        }

        private void UserControl_TouchLeave(object sender, TouchEventArgs e)
        {
            id = 0;
        }
    }
}
