﻿using ApolloDB.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Globalization;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for VideoView.xaml
    /// </summary>
    public partial class VideoView : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        DispatcherTimer timer;
        public event EventHandler EventCloseVideoView;
        public DocumentDetails documentDets { get; set; }
        public string mediaPath { get; set; } 
        #endregion

        public VideoView()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(200);
            timer.Tick += new EventHandler(timer_Tick);
        }
       

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            if (documentDets != null)
            {
                this.DataContext = documentDets;
                mediaPath = documentDets.FileLocation;
                if (!string.IsNullOrEmpty(mediaPath))
                {
                    MEVideo.Source = new System.Uri(mediaPath);
                    MEVideo.Play();
                    MEVideo.Position = new TimeSpan(0, 0, 2);
                }
                else
                    EventCloseVideoView(this, null);
            }
            Logger.Info("Completed");
        }
        private void MEVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            //close video on media end
            EventCloseVideoView(this, null);
            Logger.Info("Completed");
        }

        private void btnCloseButton_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseVideoView(this, null);
            Logger.Info("Completed");
        }

        private void btnPlay_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            MEVideo.Play();
            btnPause.Visibility = Visibility.Visible;
            btnPlay.Visibility = Visibility.Collapsed;
            Logger.Info("Completed");
        }

        private void btnPause_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            MEVideo.Pause();
            btnPause.Visibility = Visibility.Collapsed;
            btnPlay.Visibility = Visibility.Visible;
            Logger.Info("Completed");
        }

        private void btnStop_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            MEVideo.Stop();
            MEVideo.Position = new TimeSpan(0, 0, 0);
            btnPlay.Visibility = Visibility.Visible;
            btnPause.Visibility = Visibility.Collapsed;
            Logger.Info("Completed");
        }

        #region Seek Bar
        private void MEVideo_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (MEVideo.NaturalDuration.HasTimeSpan)
            {
                TimeSpan ts = MEVideo.NaturalDuration.TimeSpan;
                seekBar.Maximum = ts.TotalSeconds;
                seekBar.SmallChange = 1;
                seekBar.LargeChange = Math.Min(10, ts.Seconds / 10);
            }
            timer.Start();
        }

        bool isDragging = false;

        void timer_Tick(object sender, EventArgs e)
        {
            if (!isDragging)
            {
                seekBar.Value = MEVideo.Position.TotalSeconds;
            }
        }
        #endregion

        private void MEVideo_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            prgLoading.Visibility = Visibility.Collapsed;
            Logger.Info("Completed");
        }
    }
}
