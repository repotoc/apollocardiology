﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KIMS.UserControls.AlertControls
{
    /// <summary>
    /// Interaction logic for UnderDevelopmentUC.xaml
    /// </summary>
    public partial class UnderDevelopmentUC : UserControl
    {
        #region variables
        Timer delayTimer;
        public event EventHandler EventCloseDevelopmentUC;
        private Logger Logger = LogManager.GetCurrentClassLogger(); 
        #endregion

        public UnderDevelopmentUC()
        {
            InitializeComponent();
            delayTimer = new Timer();
            delayTimer.Interval = 2000;
            delayTimer.Elapsed += delayTimer_Elapsed;
            delayTimer.Start();
        }

        void delayTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Logger.Info("Initiated");
            this.Dispatcher.Invoke((Action)(() =>
            {
                Storyboard CloseAlert_SB = TryFindResource("CloseAlert_SB") as Storyboard;  // gets the CloseAlert_SB storyboard from resources
                CloseAlert_SB.Completed += new EventHandler(CloseAlert_SB_Completed);   //event raises when CloseAlert_SB completed
                CloseAlert_SB.Begin();    //starts CloseReviews_SB storyboard

            }));
            Logger.Info("Completed");
        }
        void CloseAlert_SB_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            delayTimer.Stop();
            delayTimer = null;
            EventCloseDevelopmentUC(this, null);
            Logger.Info("Completed");
        }
    }
}
