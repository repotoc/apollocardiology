﻿using KIMS.FeedbackQuestLogic;
using ApolloDB.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NLog;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for Feedback.xaml
    /// </summary>
    public partial class Feedback : UserControl
    {
        #region variables
        public event EventHandler EventCloseFeedback;
        public event EventHandler EventSuccessAlert;
        public event EventHandler EventWarningAlert;
        Configuration config;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        #endregion
        List<FeedbackAns> lstAnswers;
        public Feedback()
        {
            InitializeComponent();
        }

        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Apps.KillTabTip();
            Storyboard CloseFeedbackPage_SB = TryFindResource("CloseFeedbackPage_SB") as Storyboard;    //gets the CloseFeedbackPage_SB storyboard from resources
            CloseFeedbackPage_SB.Completed += new EventHandler(CloseFeedbackPage_SB_Completed); //event raises when CloseFeedbackPage_SB storyboard completed
            CloseFeedbackPage_SB.Begin();   // starts CloseFeedbackPage_SB storyboard
        }

        void CloseFeedbackPage_SB_Completed(object sender, EventArgs e)
        {
            EventCloseFeedback(this, null);  //event to close Feedback.xaml
        }

        private void scrollFeedBack_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void TextBox_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillTabTip();
        }

        private void TextBox_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            if (txtBox != null)
            {
                int questionNbr = Convert.ToInt32(txtBox.Name.Substring(txtBox.Name.Length - 2));
                if (lstAnswers.Exists(c => c.QuestionNumber.Equals(questionNbr)))
                {
                    lstAnswers.FirstOrDefault(c => c.QuestionNumber.Equals(questionNbr)).Description = txtBox.Text;
                }
            }
        }

        private void feedbackRadioButton_TouchDown(object sender, TouchEventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (rb != null)
            {
                int questionNbr = Convert.ToInt32(rb.GroupName.Substring(1));
                //int rating = Convert.ToInt32(rb.Name.Substring(rb.Name.Length - 1));
                int rating = Convert.ToInt32(rb.Content);
                if (lstAnswers.Exists(c => c.QuestionNumber.Equals(questionNbr)))
                {
                    lstAnswers.FirstOrDefault(c => c.QuestionNumber.Equals(questionNbr)).Rating = rating;
                }
            }
        }

        private void referredByRadioButton_TouchDown(object sender, TouchEventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (rb != null)
            {
                int questionNbr = Convert.ToInt32(rb.GroupName.Substring(1));
                string content = Convert.ToString(rb.Content);

                if (lstAnswers.Exists(c => c.QuestionNumber.Equals(questionNbr)))
                {
                    lstAnswers.FirstOrDefault(c => c.QuestionNumber.Equals(questionNbr)).Description = content.Equals("Others") ? txtReason05.Text : content;
                }
            }
        }

        private void dtPckr_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DatePicker dtPicker = (DatePicker)sender;
            if (dtPicker != null)
            {
                int questionNbr = Convert.ToInt32(dtPicker.Name.Substring(dtPicker.Name.Length - 2));
                if (lstAnswers.Exists(c => c.QuestionNumber.Equals(questionNbr)))
                {
                    lstAnswers.FirstOrDefault(c => c.QuestionNumber.Equals(questionNbr)).EntryDate = dtPicker.SelectedDate;
                }
            }
        }
        string strUHID = string.Empty, strMobile = string.Empty, strEmail = string.Empty;
        private void SendMail(string mailString, string userName, string strUHID, string strMobile, string strEmail)
        {
            Logger.Info("Initiated");
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            string email = config.AppSettings.Settings["EmailID"].Value;
            string EmailIDUser1 = config.AppSettings.Settings["EmailIDUser1"].Value;
            string EmailIDUser2 = config.AppSettings.Settings["EmailIDUser2"].Value;
            string password = config.AppSettings.Settings["EmailPassword"].Value;
            string smtp = config.AppSettings.Settings["SMTP"].Value;
            string port = config.AppSettings.Settings["Port"].Value;

            if (!string.IsNullOrEmpty(EmailIDUser1))
            {
                string Message = string.Empty;
                try
                {
                    MailMessage mail = new MailMessage(email, EmailIDUser1);
                    if (!string.IsNullOrEmpty(EmailIDUser2))
                    {
                        mail.To.Add(EmailIDUser2);
                    }
                    SmtpClient sc = new SmtpClient(smtp, Convert.ToInt32(port));

                    mail.Subject = "Apollo Cardiology User Feedback Mail";
                    mail.IsBodyHtml = true;
                    mail.Body = GenerateBody(mailString, userName, strUHID, strMobile, strEmail);
                    sc.Credentials = new System.Net.NetworkCredential(email, password);
                    sc.EnableSsl = true;
                    sc.Send(mail);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, ex.Message);
                }
            }
            Logger.Info("Completed");
        }

        private string GenerateBody(string mailString, string userName, string strUHID, string strMobile, string strEmail)
        {
            Logger.Info("Initiated");
            string body = string.Empty;

            string configPath = ConfigurationManager.AppSettings["EmailTemplatePath"];
            string templatePath = System.IO.Path.Combine(Apps.strPath, configPath);

            if (File.Exists(templatePath))
            {
                string mailbody = System.IO.File.ReadAllText(templatePath);
                using (StreamReader reader = new StreamReader(templatePath))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{CustormerName}", userName);
                body = body.Replace("{FeedbackData}", mailString);
                if (!string.IsNullOrEmpty(strMobile))
                {
                    body = body.Replace("{Mobile}", "Mobile : " + strMobile);
                }
                else { body = body.Replace("{Mobile}", string.Empty); }
                if (!string.IsNullOrEmpty(strEmail))
                {
                    body = body.Replace("{Email}", "Email : " + strEmail);
                }
                else { body = body.Replace("{Email}", string.Empty); }
                if (!string.IsNullOrEmpty(strUHID))
                {
                    body = body.Replace("{UHID}", "UHID : " + strUHID);
                }
                else { body = body.Replace("{UHID}", string.Empty); }
            }
            Logger.Info("Completed");
            return body;
        }

        private void btnSubmit_TouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                if (Apps.PingCheck())
                {
                    StringBuilder sb = new StringBuilder();
                    if (lstAnswers.Where(c => c.Rating > 0 || !string.IsNullOrEmpty(c.Description) || c.EntryDate != null).ToList().Count > 1)
                    {
                        lstAnswers.Where(c => c.Rating > 0 || !string.IsNullOrEmpty(c.Description) || c.EntryDate != null).ToList().ForEach(c =>
                        {                            
                            ApolloDB.Models.FeedBackQuestions question = Apps.myDbContext.FEEDBACKQUESTIONS.FirstOrDefault(d => d.ID.Equals(c.QuestionNumber));
                            if (question != null)
                            {
                                ApolloDB.Models.FeedBackSubmits submit = new ApolloDB.Models.FeedBackSubmits();
                                submit.FIRSTTIME = txtName.Text;
                                submit.ID = Apps.myDbContext.FEEDBACKSUBMITS.ToList().Count > 0 ? Apps.myDbContext.FEEDBACKSUBMITS.Max(d => d.ID) + 1 : 1;
                                submit.LASTUPDATEDTIME = DateTime.Now;
                                submit.MOBILENUMBER = txtMobileNo.Text;
                                submit.NAME = txtName.Text;
                                submit.ADDRESS = c.Description;
                                submit.RATING = c.Rating;
                                submit.UHIDNUMBER = txtUHIDNo.Text;
                                submit.VISITDATE = c.EntryDate != null ? c.EntryDate : DateTime.Now;
                                submit.FEEDBACKQUEST = question;
                                question.LSTFEEDBACKS.Add(submit);
                                Apps.myDbContext.FEEDBACKSUBMITS.Add(submit);
                                Apps.myDbContext.SaveChanges();

                                string framedString = string.Empty;
                                framedString += c.Rating > -1 ? "Rating:" + c.Rating : string.Empty;
                                framedString += (string.IsNullOrEmpty(c.Description)) ? string.Empty : "<br/>Desc: " + c.Description;
                                framedString += c.EntryDate == null ? string.Empty : "<br/>Date: " + Convert.ToDateTime(c.EntryDate).ToShortDateString();

                                sb.AppendFormat("<tr><td style='padding: 5px;width: 70%;border-left: 1px solid #f4f4f4;border-bottom: 1px solid #f4f4f4;'>{0}</td><td style='padding:5px;border-left: 1px solid #f4f4f4;border-bottom: 1px solid #f4f4f4;'>{1}</td></tr>", question.QUESTION, framedString);
                            }
                        });
                        
                        string mailString = sb.ToString();
                        string userName = txtName.Text;
                        string strUHID = txtUHIDNo.Text;
                        string strMobile = txtMobileNo.Text;
                        string strEmail = txtEMailAddress.Text;
                        Task.Factory.StartNew(() =>
                        {
                            SendMail(mailString, userName, strUHID, strMobile, strEmail);
                        });
                        string Msg = "Thanks for submitting Feedback !!!";
                        EventSuccessAlert(Msg, null);
                        EventCloseFeedback(this, null);
                    }
                    else
                    {
                        string Msg = "Please answer atleast one !!!";
                        EventWarningAlert(Msg, null);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("FeedbackUC");
            lstAnswers = new List<FeedbackAns>();
            for (int i = 0; i < 6; i++)
            {
                lstAnswers.Add(new FeedbackAns(i + 1));
            }
            Logger.Info("Completed");
        }
    }
}
