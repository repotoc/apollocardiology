﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for FloorIndex.xaml
    /// </summary>
    public partial class FloorIndex : UserControl
    {
        public FloorIndex()
        {
            InitializeComponent();
        }
        #region variables
        public event EventHandler EventCloseFloorIndex;
        private Logger Logger = LogManager.GetCurrentClassLogger(); 
        #endregion

        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseFloorIndex(this, null);
            Logger.Info("Completed");
        }
    }
}
