﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Maps.MapControl.WPF;
using NLog;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for ContactUC.xaml
    /// </summary>
    public partial class ContactUC : UserControl
    {
        #region variables
        public event EventHandler EventCloseContactUC;
        private Logger Logger = LogManager.GetCurrentClassLogger();  
        #endregion
        public ContactUC()
        {
            InitializeComponent();
        }
        private void CreatePushPins()
        {
            Logger.Info("Initiated");
            double Latitude = 17.414868;
            double Longitude = 78.412342;
            Pushpin pin = CreatePushPin(Latitude, Longitude);
            if (pin != null)
            {
                map.Children.Add(pin);
            }
            Logger.Info("Completed");
        }

        private Pushpin CreatePushPin(double Latitude, double Longitude)
        {
            Logger.Info("Initiated");
            //if (Latitude != null && Longitude != null)
            //{
                Pushpin pin = new Pushpin();
                pin.Location = new Location(Convert.ToDouble(Latitude), Convert.ToDouble(Longitude));
                Logger.Info("Completed");
                return pin;
            //}
            //return null;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            CreatePushPins();
            Logger.Info("Completed");
        }

        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseContactUC(this, null);
            Logger.Info("Completed");
        }

        private void svContactDetails_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
