﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for InternetDown.xaml
    /// </summary>
    public partial class InternetDown : Window
    {
        public InternetDown()
        {
            InitializeComponent();
        }
        #region variables
        public event EventHandler EventCloseConnection;
        private Logger Logger = LogManager.GetCurrentClassLogger();
        DispatcherTimer timer; 
        #endregion
        private void btnCloseWifi_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseConnection(this, null);
            Logger.Info("Completed");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 3);
            timer.Tick += idleTimer_Tick;
            timer.Start();
            Logger.Info("Completed");
        }
        void idleTimer_Tick(object sender, EventArgs e)
        {
            if (Apps.PingCheck())
            {
                timer.Stop();
                this.Close();
            }
        }
    }
}
