﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Media.Animation;
using System.Configuration;
using System.Globalization;
using NLog;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for SlideShowUC.xaml
    /// </summary>
    public partial class SlideShowUC : UserControl
    {
        #region Variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        bool AdminPanelLoaded = false;
        private TouchPoint TouchStart;

        private DispatcherTimer timerImageChange;
        private Image[] ImageControls;
        private List<ImageSource> Images = new List<ImageSource>();
        private static string[] ValidImageExtensions = new[] { ".png", ".jpg", ".jpeg" };//, ".jpg", ".jpeg", ".bmp", ".gif" };
        private static string[] TransitionEffects = new[] { "Fade" };
        private string TransitionType, strImagePath = "";
        private int CurrentSourceIndex, CurrentCtrlIndex, EffectIndex = 0, IntervalTimer = 1;

        public event EventHandler EventOpenAdminSettings;
        public event EventHandler EventCloseSlideShow;
        #endregion
        public SlideShowUC()
        {
            InitializeComponent();
            //Initialize Image control, Image directory path and Image timer.
            IntervalTimer = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalTime"]);
            strImagePath = ConfigurationManager.AppSettings["ImagePath"];
            ImageControls = new[] { myImage, myImage2 };

            LoadImageFolder(strImagePath);

            timerImageChange = new DispatcherTimer();
            timerImageChange.Interval = new TimeSpan(0, 0, IntervalTimer);
            timerImageChange.Tick += new EventHandler(timerImageChange_Tick);
        }
        
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            PlaySlideShow();
            timerImageChange.IsEnabled = true;
            Logger.Info("Completed");
        }
        private void LoadImageFolder(string folder)
        {
            Logger.Info("Initiated");
            ErrorText.Visibility = Visibility.Collapsed;
            var sw = System.Diagnostics.Stopwatch.StartNew();
            if (!System.IO.Path.IsPathRooted(folder))
                folder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, folder); //System.IO.Path.Combine(Environment.CurrentDirectory, folder);
            if (!System.IO.Directory.Exists(folder))
            {
                ErrorText.Text = "Please Upload Home Slide Images to display "; // + Environment.NewLine + folder;
                ErrorText.Visibility = Visibility.Visible;
                return;
            }
            Random r = new Random();
            var sources = from file in new System.IO.DirectoryInfo(folder).GetFiles().AsParallel()
                          where ValidImageExtensions.Contains(file.Extension, StringComparer.InvariantCultureIgnoreCase)
                          orderby r.Next()
                          select CreateImageSource(file.FullName, true);
            Images.Clear();
            Images.AddRange(sources);
            sw.Stop();
            Console.WriteLine("Total time to load {0} images: {1}ms", Images.Count, sw.ElapsedMilliseconds);
            Logger.Info("Completed");
        }

        private static ImageSource CreateImageSource(string file, bool forcePreLoad)
        {
            if (forcePreLoad)
            {
                var src = new BitmapImage();
                src.BeginInit();
                src.UriSource = new Uri(file, UriKind.Absolute);
                src.CacheOption = BitmapCacheOption.OnLoad;
                src.EndInit();
                src.Freeze();
                return src;
            }
            else
            {
                var src = new BitmapImage(new Uri(file, UriKind.Absolute));
                src.Freeze();
                return src;
            }
        }

        private void timerImageChange_Tick(object sender, EventArgs e)
        {
            PlaySlideShow();
        }

        private void PlaySlideShow()
        {
            try
            {
                if (Images.Count == 0)
                {
                    return;
                }
                var oldCtrlIndex = CurrentCtrlIndex;
                CurrentCtrlIndex = (CurrentCtrlIndex + 1) % 2;
                CurrentSourceIndex = (CurrentSourceIndex + 1) % Images.Count;

                Image imgFadeOut = ImageControls[oldCtrlIndex];
                Image imgFadeIn = ImageControls[CurrentCtrlIndex];
                ImageSource newSource = Images[CurrentSourceIndex];
                imgFadeIn.Source = newSource;

                TransitionType = TransitionEffects[EffectIndex].ToString();

                Storyboard StboardFadeOut = (Resources[string.Format("{0}Out", TransitionType.ToString(CultureInfo.CurrentUICulture))] as Storyboard).Clone();
                StboardFadeOut.Begin(imgFadeOut);
                Storyboard StboardFadeIn = Resources[string.Format("{0}In", TransitionType.ToString(CultureInfo.CurrentUICulture))] as Storyboard;
                StboardFadeIn.Begin(imgFadeIn);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        private void MainLayout_TouchDown(object sender, TouchEventArgs e)
        {
            AdminPanelLoaded = false;
            TouchStart = e.GetTouchPoint(this);
        }
        void MainLayout_TouchMove(object sender, TouchEventArgs e)
        {
            if (!AdminPanelLoaded)
            {
                var Touch = e.GetTouchPoint(this);
                if (TouchStart != null && TouchStart.Position.X > 1000 && Touch.Position.X > 1000)
                {
                    if (Touch.Position.X < (TouchStart.Position.X - 50))
                    {
                        Logger.Info("Initiated");
                        EventOpenAdminSettings(this, null);
                        AdminPanelLoaded = true;
                        Logger.Info("Completed");
                    }
                }
                e.Handled = true;
            }
        }

        private void btntmh_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            timerImageChange.Stop();
            timerImageChange = null;
            EventCloseSlideShow(this, null);
            Logger.Info("Completed");
        }
    }
}
