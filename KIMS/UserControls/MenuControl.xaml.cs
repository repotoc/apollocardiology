﻿using ApolloDB.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for MenuControl.xaml
    /// </summary>
    public partial class MenuControl : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        private bool isScrollMoved = false;
        private Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        public event EventHandler EventOpenMenuItem; 
        #endregion

        public MenuControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            List<Category> categ = Apps.myDbContext.CATEGORY.Where(c=>c.IsActive).OrderBy(d=>d.Order).ToList();
            ICMenuList.ItemsSource = categ;
            Logger.Info("Completed");
        }

        private void btnMenuItem_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            try
            {
                Button btn = sender as Button;
                btnTouchedItem = btn;
                if (!isScrollMoved)
                {

                }
                else
                    isScrollMoved = false;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            Logger.Info("Completed");
        }

        private void svMenu_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void svMenu_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svMenu_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svMenu_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svMenu_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    if (btnTouchedItem != null && btnTouchedItem.DataContext != null)
                    {
                        Logger.Info("Initiated");
                        Category category = btnTouchedItem.DataContext as Category;
                        if (category != null)
                        {
                            EventOpenMenuItem(category, null);
                        }
                    }
                }
            }
        }
    }
}
