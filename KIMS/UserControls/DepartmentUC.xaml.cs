﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using ApolloDB.Models;
using NLog;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for DepartmentUC.xaml
    /// </summary>
    public partial class DepartmentUC : UserControl
    {        
        public DepartmentUC()
        {
            InitializeComponent();
        }
        #region Variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseDepartment;
        public event EventHandler EventOpenDoctorProfile;
        Button btnDoctorTouchedItem = null;
        Button btnDeptLeftTouchedItem = null;
        Button btnDeptRightTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        bool isScrollMoved = false;
        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            int count = 1;
            List<Department> lstLeftDepartments = new List<Department>();
            List<Department> lstRightDepartments = new List<Department>();
                       
            
            Apps.myDbContext.DEPARTMENTS.Where(b=>b.IsActive).OrderBy(b=>b.Priority).ToList().ForEach(c =>
            {
                if (count <= 13)
                {
                    lstLeftDepartments.Add(c);
                    count++;
                }
                else
                {
                    lstRightDepartments.Add(c);
                    count++;
                }
            });
            DepartmentLeftContext.ItemsSource = lstLeftDepartments;
            DepartmentRightContext.ItemsSource = lstRightDepartments;
            if (lstLeftDepartments.Count > 0)
            {
                Department deptFirstItem = lstLeftDepartments.First();
                LoadDoctorsList(deptFirstItem);
            }
            //TopCategory topCateg = Apps.dbContext.TOPCATEGORY.FirstOrDefault(c => c.NAME.Equals(TopEnums.DEPARTMENTS.ToString()));

            //if (topCateg != null && topCateg.LSTDEPARTMENTS != null && topCateg.LSTDEPARTMENTS.Count > 0)
            //{
            //    Departments department = topCateg.LSTDEPARTMENTS[24];
            //    if (department != null)
            //    {
            //        tblDepartmentName.Text = department.NAME.ToUpper() + " DEPARTMENT";
            //        grdDoctors.DataContext = department.LSTDOCTORS.OrderBy(c => c.ID).Where(c => c.ISACTIVE);
            //    }
            //}
            Logger.Info("Completed");
        }

        
        private void btnDepartment_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Button btn = sender as Button;
            btnDeptLeftTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
            Logger.Info("Completed");
        }

        private void btnCloseTeam_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseDepartment(this, null);
            Logger.Info("Completed");
        }

        #region DoctorScrollRegion

        private void btnDoctors_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Button btn = sender as Button;
            btnDoctorTouchedItem = btn;
            if (!isScrollMoved)
            {
            }
            else
                isScrollMoved = false;
            Logger.Info("Completed");
        }
        private void svDoctors_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void svDoctors_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svDoctors_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnDoctorTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svDoctors_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svDoctors_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    if (btnDoctorTouchedItem != null && btnDoctorTouchedItem.DataContext != null)
                    {
                        Logger.Info("Initiated");
                        Doctor doc = btnDoctorTouchedItem.DataContext as Doctor;
                        if (doc != null)
                        {
                            EventOpenDoctorProfile(doc, null);
                        }
                        Logger.Info("Completed");
                    }
                }
            }
        }
        #endregion

        #region DepartmentLeftScrollRegion
        private void svLeftDocList_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        private void svLeftDocList_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReportedLeftDept;
            Touch.FrameReported += Touch_FrameReportedLeftDept;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReportedLeftDept(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svLeftDocList_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnDeptLeftTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svLeftDocList_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svLeftDocList_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    if (btnDeptLeftTouchedItem != null && btnDeptLeftTouchedItem.DataContext != null)
                    {
                        Logger.Info("Initiated");
                        Department dept = btnDeptLeftTouchedItem.DataContext as Department;
                        if (dept != null)
                        {
                            LoadDoctorsList(dept);
                        }
                        Logger.Info("Completed");
                    }
                }
            }
        }
        #endregion

        #region DepartmentRightScrollRegion
        private void btnDepartmentRight_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Button btn = sender as Button;
            btnDeptRightTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
            Logger.Info("Completed");
        }

        private void svRightDocList_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void svRightDocList_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReportedRightDept;
            Touch.FrameReported += Touch_FrameReportedRightDept;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReportedRightDept(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svRightDocList_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnDeptRightTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svRightDocList_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svRightDocList_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    if (btnDeptRightTouchedItem != null && btnDeptRightTouchedItem.DataContext != null)
                    {
                        Logger.Info("Initiated");
                        Department dept = btnDeptRightTouchedItem.DataContext as Department;
                        if (dept != null)
                        {
                            LoadDoctorsList(dept);
                        }
                        Logger.Info("Completed");
                    }
                }
            }
        }
                
        #endregion

        private void LoadDoctorsList(Department dept)
        {
            Logger.Info("Initiated");
            if (dept != null)
            {
                DoctorsContext.ItemsSource = null;
                tbDepatmentName.Text = dept.Name.ToUpper(CultureInfo.CurrentCulture);
                if (dept.LstDoctors != null && dept.LstDoctors.Count > 0)
                {
                    DoctorsContext.ItemsSource = dept.LstDoctors.Where(c => c.IsActive).OrderBy(d=>d.Order).ToList();
                }
            }
            Logger.Info("Completed");
        }       
    }
}
