﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApolloDB.Models;
using System.Windows.Media.Animation;
using NLog;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for ImageView.xaml
    /// </summary>
    public partial class ImageView : UserControl
    {
        public ImageView()
        {
            InitializeComponent();
        }
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public DocumentDetails documentDets { get; set; }
        public event EventHandler EventCloseImageView; 
        #endregion

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Storyboard Close_SB = TryFindResource("Close_SB") as Storyboard;
            Close_SB.Completed += new EventHandler(Close_SB_Completed);
            Close_SB.Begin();
            Logger.Info("Completed");
        }

        void Close_SB_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseImageView(this, null);
            Logger.Info("Completed");
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            this.DataContext = documentDets;
            Logger.Info("Completed");
        }
    }
}
