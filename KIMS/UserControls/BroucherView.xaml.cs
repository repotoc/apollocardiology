﻿using ApolloDB.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for BroucherView.xaml
    /// </summary>
    public partial class BroucherView : UserControl
    {
        public BroucherView()
        {
            InitializeComponent();
        }

        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseBroucher;
        private int imageStartIndex = 0;
        private bool isNextActivated = false;
        private bool isPreviousActivated = false;
        public List<DocumentDetails> listCategoryData; 
        #endregion
        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseBroucher(this, null);
            Logger.Info("Completed");
        }


        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            imageStartIndex = 0;
            
            listCategoryData.Insert(0, new DocumentDetails() { FileLocation = "../Icons/Fcover.png", ImageExtension = "png", WebFileLocation = "../Icons/Fcover.png" });
            listCategoryData.Add(new DocumentDetails() { FileLocation = "../Icons/ThankYou.png", ImageExtension = "png", WebFileLocation = "../Icons/ThankYou.png" });
            viewportRight.DataContext = listCategoryData[imageStartIndex].FileLocation;
            Logger.Info("Completed");
        }

        private void btnNext_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (listCategoryData.Count > 1)
            {
                LoadNextButtonData();
            }
            Logger.Info("Completed");
        }

        private void btnPrevious_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (listCategoryData.Count > 1)
            {
                LoadPreviousButtonData();
            }
            Logger.Info("Completed");
        }

        private void LoadNextButtonData()
        {
            Logger.Info("Initiated");
            if (isPreviousActivated)
            {
                isPreviousActivated = false;
                imageStartIndex += 1;
            }
            if (imageStartIndex < listCategoryData.Count - 1)
            {
                isNextActivated = true;
                if (ContinueToNext(imageStartIndex))
                {
                    viewportRightFlipUp.DataContext = listCategoryData[imageStartIndex].FileLocation;
                    if (ContinueToNext(++imageStartIndex))
                    {
                        viewportLeftFlipDown.DataContext = listCategoryData[imageStartIndex].FileLocation;
                        if (ContinueToNext(++imageStartIndex))
                        {
                            viewportRight.DataContext = listCategoryData[imageStartIndex].FileLocation;
                        }
                        else
                        {
                            viewportRight.DataContext = "../Icons/Bcover.png";
                        }
                    }
                    else
                    {
                        LoadBrocherEndPage();
                    }
                }
                else
                {
                    LoadBrocherEndPage();
                }
                Storyboard nxtSB = this.TryFindResource("SB_Next") as Storyboard;
                nxtSB.Completed -= new EventHandler(nxtSB_Completed);
                nxtSB.Completed += new EventHandler(nxtSB_Completed);
                nxtSB.Begin();
            }
            else
            {
                viewportLeftFlipUp.DataContext = null;
                viewportRightFlipUp.DataContext = null;
                viewportRightFlipDown.DataContext = null;
            }
            Logger.Info("Completed");
        }

        private void LoadPreviousButtonData()
        {
            Logger.Info("Initiated");
            if (isNextActivated)
            {
                isNextActivated = false;
                imageStartIndex -= 1;
            }
            if (imageStartIndex > 0)
            {
                isPreviousActivated = true;
                if (ContinueToPrevious(imageStartIndex))
                {
                    viewportLeftFlipUp.DataContext = listCategoryData[imageStartIndex].FileLocation;
                    if (ContinueToPrevious(--imageStartIndex))
                    {
                        viewportRightFlipDown.DataContext = listCategoryData[imageStartIndex].FileLocation;
                        if (ContinueToPrevious(--imageStartIndex))
                        {
                            viewportLeft.DataContext = listCategoryData[imageStartIndex].FileLocation;
                        }
                        else
                        {
                            viewportLeft.DataContext = null;
                        }
                    }
                    else
                    {
                        LoadBrocherStartPage();
                    }
                }
                else
                {
                    LoadBrocherStartPage();
                }
                if (imageStartIndex == 0)
                {
                    viewportLeft.Visibility = Visibility.Collapsed;
                }
                Storyboard prevSB = this.TryFindResource("SB_Previous") as Storyboard;
                prevSB.Completed -= new EventHandler(prevSB_Completed);
                prevSB.Completed += new EventHandler(prevSB_Completed);
                prevSB.Begin();
            }
            else
            {
                viewportLeftFlipUp.DataContext = null;
                viewportLeftFlipDown.DataContext = null;
                viewportRightFlipUp.DataContext = null;
                viewportRightFlipDown.DataContext = null;
                viewportLeft.DataContext = null;
            }
            Logger.Info("Completed");
        }

        private void LoadBrocherStartPage()
        {
            Logger.Info("Initiated");
            if (imageStartIndex > -1)
            {
                viewportRight.DataContext = listCategoryData[imageStartIndex].FileLocation;
                viewportLeft.DataContext = null;
            }
            Logger.Info("Completed");
        }

        void nxtSB_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            if (ContinueToNext(imageStartIndex))
            {
                viewportLeft.DataContext = listCategoryData[imageStartIndex].FileLocation;
            }
            Logger.Info("Completed");
        }

        private void prevSB_Completed(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            if (imageStartIndex >= 0)
            {
                viewportRight.DataContext = listCategoryData[imageStartIndex + 1].FileLocation;
            }

            if (imageStartIndex <= 0)
            {
                viewportRight.DataContext = listCategoryData[0].FileLocation;
                viewportLeft.DataContext = null;
            }
            Logger.Info("Completed");
        }

        private bool ContinueToNext(int imageIndex)
        {
            Logger.Info("Initiated");
            if (imageIndex < 0)
            {
                imageIndex = 0;
                imageStartIndex = 0;
            }
            Logger.Info("Completed");
            return (imageIndex <= listCategoryData.Count - 1) ? true : false;
        }

        private bool ContinueToPrevious(int imageIndex)
        {
            Logger.Info("Initiated");
            if (imageIndex == listCategoryData.Count)
            {
                imageStartIndex = listCategoryData.Count - 1;
                imageIndex = imageStartIndex;
            }
            Logger.Info("Completed");
            return (imageIndex < listCategoryData.Count && imageIndex >= 0) ? true : false;
        }

        private void LoadBrocherEndPage()
        {
            Logger.Info("Initiated");
            viewportRightFlipUp.DataContext = listCategoryData[imageStartIndex - 2].FileLocation;
            viewportLeftFlipDown.DataContext = listCategoryData[imageStartIndex - 1].FileLocation;
            viewportLeft.DataContext = listCategoryData[imageStartIndex - 1].FileLocation;
            viewportRight.DataContext = null;
            Logger.Info("Completed");
        }
    }
}
