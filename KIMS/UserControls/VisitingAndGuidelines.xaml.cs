﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KIMS.UserControls
{
    /// <summary>
    /// Interaction logic for VisitingAndGuidelines.xaml
    /// </summary>
    public partial class VisitingAndGuidelines : UserControl
    {
        public VisitingAndGuidelines()
        {
            InitializeComponent();
        }

        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventCloseVisitingHoursAndGuidelines; 
        #endregion
        
        private void btnCloseButton_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseVisitingHoursAndGuidelines(this, null);
            Logger.Info("Completed");
        }
    }
}
