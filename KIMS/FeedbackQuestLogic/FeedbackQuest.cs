﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIMS.FeedbackQuestLogic
{
    public class FeedbackQuest
    {
        public string Name { get; set; }
        public FeedbackDisplayType DisplayType { get; set; }
    }
    public class FeedbackCategory
    {
        public FeedbackCategory()
        {
            LstFeedbackQuests = new List<FeedbackQuest>();
        }
        public string Name { get; set; }
        public List<FeedbackQuest> LstFeedbackQuests { get; set; }
    }

    public class PredefinedQuestions
    {
        public const string FirstQuest = "How satisfied are you with your experience and the services provided by our hospital on a scale of 0  to 10 ?";
        public const string SecondQuest = "How likely would you recommend our hospital to your friends &amp; dear ones, if necessary ?";
        public const string ThirdQuest = "Did you identify a service star (someone whose services were excellent ) ?";
        public const string FourthQuest = "Have you used our service before ?"; 
        public const string FifthQuest = "Referred by: Family/Friend/Others (Specify) :";
        public const string SixthQuest = "Would you like to get update on subjects like health &amp; lifestyle ?";
    }

    public class PredefinedCategory
    {
        public const string Empty = " ";
    }
}
