﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIMS.FeedbackQuestLogic
{
    public class FeedbackAns
    {
        public FeedbackAns(int questionNbr)
        {
            this.QuestionNumber = questionNbr;
            this.Rating = -1;
            this.Description = string.Empty;
            this.EntryDate = null;
        }

        public int QuestionNumber { get; set; }
        public int Rating { get; set; }
        public string Description { get; set; }
        public DateTime? EntryDate { get; set; }
    }
}
