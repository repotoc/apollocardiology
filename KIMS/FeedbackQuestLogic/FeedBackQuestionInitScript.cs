﻿using ApolloDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace KIMS.FeedbackQuestLogic
{
    public class FeedBackQuestionInitScript
    {
        public List<FeedbackCategory> LstFeedbackQuest { get; set; }
        private Logger Logger = LogManager.GetCurrentClassLogger();

        public FeedBackQuestionInitScript()
        {
            LstFeedbackQuest = LoadQuestions();
        }

        private List<FeedbackCategory> LoadQuestions()
        {
            List<FeedbackCategory> lstCategories = new List<FeedbackCategory>();

            #region Empty Category
            FeedbackCategory emptycategory = new FeedbackCategory() { Name = PredefinedCategory.Empty };
            emptycategory.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.FirstQuest, DisplayType = FeedbackDisplayType.RatingWithReasons });
            emptycategory.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.SecondQuest, DisplayType = FeedbackDisplayType.RatingWithReasons });
            emptycategory.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.ThirdQuest, DisplayType = FeedbackDisplayType.QuestionWithTextBox });
            emptycategory.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.FourthQuest, DisplayType = FeedbackDisplayType.QuestionWithBooleanAndDate });
            emptycategory.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.FifthQuest, DisplayType = FeedbackDisplayType.QuestionWithTextBox });
            emptycategory.LstFeedbackQuests.Add(new FeedbackQuest() { Name = PredefinedQuestions.SixthQuest, DisplayType = FeedbackDisplayType.QuestionWithBoolean });
            lstCategories.Add(emptycategory);
            #endregion

            return lstCategories;
        }

        public void RunInitScript()
        {
            Initiate();
        }

        private void Initiate()
        {
            long maxCateg = Apps.myDbContext.FEEDBACKCATEGORY.ToList().Count > 0 ? Apps.myDbContext.FEEDBACKCATEGORY.Max(e => e.ID) + 1 : 1;
            long maxFeedbackQuest = Apps.myDbContext.FEEDBACKQUESTIONS.ToList().Count > 0 ? Apps.myDbContext.FEEDBACKQUESTIONS.Max(e => e.ID) + 1 : 1;

            try
            {
                LstFeedbackQuest.ForEach(c =>
                {
                    ApolloDB.Models.FeedbackCategory masterCateg = Apps.myDbContext.FEEDBACKCATEGORY.FirstOrDefault(d => d.NAME.Equals(c.Name, StringComparison.InvariantCultureIgnoreCase));
                    if (masterCateg == null)
                    {
                        masterCateg = PrepareFeedbackCategory(maxCateg, c);
                        maxCateg++;
                        Apps.myDbContext.FEEDBACKCATEGORY.Add(masterCateg);
                    }

                    c.LstFeedbackQuests.ForEach(d =>
                    {
                        if (!Apps.myDbContext.FEEDBACKQUESTIONS.Any(e => e.QUESTION.Equals(d.Name, StringComparison.InvariantCultureIgnoreCase)))
                        {
                            ApolloDB.Models.FeedBackQuestions question = PrepareFeedbackQuestion(maxFeedbackQuest, d);
                            maxFeedbackQuest++;
                            if (masterCateg.LSTFEEDBACKQUESTIONS == null)
                            {
                                masterCateg.LSTFEEDBACKQUESTIONS = new List<ApolloDB.Models.FeedBackQuestions>();
                            }
                            masterCateg.LSTFEEDBACKQUESTIONS.Add(question);
                        }
                    });

                    Apps.myDbContext.SaveChanges();
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        private FeedBackQuestions PrepareFeedbackQuestion(long maxFeedbackQuest, FeedbackQuest d)
        {
            FeedBackQuestions question = new FeedBackQuestions();
            question.QUESTION = d.Name;
            question.DESCRIPTION = string.Empty;
            question.ID = maxFeedbackQuest;
            question.ISACTIVE = true;
            question.LASTUPDATEDTIME = DateTime.Now;
            question.VERSION = 1;
            return question;
        }

        private ApolloDB.Models.FeedbackCategory PrepareFeedbackCategory(long maxCateg, FeedbackCategory c)
        {
            ApolloDB.Models.FeedbackCategory category = new ApolloDB.Models.FeedbackCategory();
            category.NAME = c.Name;
            category.ID = maxCateg;
            category.ISACTIVE = true;
            category.LASTUPDATEDTIME = DateTime.Now;
            category.VERSION = 1;
            return category;
        }
    }
}
