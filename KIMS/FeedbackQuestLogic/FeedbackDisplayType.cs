﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KIMS.FeedbackQuestLogic
{
    public enum FeedbackDisplayType
    {
        Rating,
        RatingWithReasons,
        QuestionWithTextBox,
        QuestionWithBoolean,
        QuestionWithBooleanAndDate,
        QuestionWithDate
    }
}
