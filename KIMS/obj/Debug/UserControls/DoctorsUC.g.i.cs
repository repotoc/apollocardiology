﻿#pragma checksum "..\..\..\UserControls\DoctorsUC.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "4C664DCBECA25DC25C67571C94CF8B74"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KIMS.UserControls {
    
    
    /// <summary>
    /// DoctorsUC
    /// </summary>
    public partial class DoctorsUC : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 19 "..\..\..\UserControls\DoctorsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCloseButton;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\UserControls\DoctorsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer svLeftDocList;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\UserControls\DoctorsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl DepartmentsContext;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\UserControls\DoctorsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdDoctors;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\UserControls\DoctorsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tblDepartmentName;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\UserControls\DoctorsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer svDocList2;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\UserControls\DoctorsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl DoctorsContext;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\..\UserControls\DoctorsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer svRightDocList;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\UserControls\DoctorsUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl DepartmentsRightContext;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/KIMS;component/usercontrols/doctorsuc.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\DoctorsUC.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 7 "..\..\..\UserControls\DoctorsUC.xaml"
            ((KIMS.UserControls.DoctorsUC)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnCloseButton = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\..\UserControls\DoctorsUC.xaml"
            this.btnCloseButton.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnCloseButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 3:
            this.svLeftDocList = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 34 "..\..\..\UserControls\DoctorsUC.xaml"
            this.svLeftDocList.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.sv_ManipulationBoundaryFeedback);
            
            #line default
            #line hidden
            return;
            case 4:
            this.DepartmentsContext = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 6:
            this.grdDoctors = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.tblDepartmentName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.svDocList2 = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 72 "..\..\..\UserControls\DoctorsUC.xaml"
            this.svDocList2.ScrollChanged += new System.Windows.Controls.ScrollChangedEventHandler(this.svDocList2_ScrollChanged);
            
            #line default
            #line hidden
            
            #line 73 "..\..\..\UserControls\DoctorsUC.xaml"
            this.svDocList2.PreviewTouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.svDocList2_PreviewTouchDown);
            
            #line default
            #line hidden
            
            #line 74 "..\..\..\UserControls\DoctorsUC.xaml"
            this.svDocList2.PreviewTouchMove += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.svDocList2_PreviewTouchMove);
            
            #line default
            #line hidden
            
            #line 75 "..\..\..\UserControls\DoctorsUC.xaml"
            this.svDocList2.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.svDocList2_PreviewTouchUp);
            
            #line default
            #line hidden
            
            #line 76 "..\..\..\UserControls\DoctorsUC.xaml"
            this.svDocList2.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.svDocList2_ManipulationBoundaryFeedback);
            
            #line default
            #line hidden
            return;
            case 9:
            this.DoctorsContext = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 11:
            this.svRightDocList = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 142 "..\..\..\UserControls\DoctorsUC.xaml"
            this.svRightDocList.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.sv_ManipulationBoundaryFeedback);
            
            #line default
            #line hidden
            return;
            case 12:
            this.DepartmentsRightContext = ((System.Windows.Controls.ItemsControl)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 5:
            
            #line 40 "..\..\..\UserControls\DoctorsUC.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnDepartment_TouchDown);
            
            #line default
            #line hidden
            break;
            case 10:
            
            #line 80 "..\..\..\UserControls\DoctorsUC.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnDocProfile_TouchDown);
            
            #line default
            #line hidden
            break;
            case 13:
            
            #line 146 "..\..\..\UserControls\DoctorsUC.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnDepartment_TouchDown);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

