﻿#pragma checksum "..\..\..\UserControls\Feedback.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "02772BAED45475EF1CA03AB96928D1A3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using KIMS.Converters;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KIMS.UserControls {
    
    
    /// <summary>
    /// Feedback
    /// </summary>
    public partial class Feedback : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 147 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrdMainLayout;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border MainLayout;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grdFeedbackForm;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCloseTeam;
        
        #line default
        #line hidden
        
        
        #line 179 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GridForm;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer svFeedback;
        
        #line default
        #line hidden
        
        
        #line 227 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtReason01;
        
        #line default
        #line hidden
        
        
        #line 248 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtReason02;
        
        #line default
        #line hidden
        
        
        #line 256 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtServiceQuest03;
        
        #line default
        #line hidden
        
        
        #line 262 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUHIDNo;
        
        #line default
        #line hidden
        
        
        #line 265 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtName;
        
        #line default
        #line hidden
        
        
        #line 269 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dtCheckupDate;
        
        #line default
        #line hidden
        
        
        #line 272 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtMobileNo;
        
        #line default
        #line hidden
        
        
        #line 283 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dtPcker04;
        
        #line default
        #line hidden
        
        
        #line 291 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtReason05;
        
        #line default
        #line hidden
        
        
        #line 304 "..\..\..\UserControls\Feedback.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEMailAddress;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ApolloCardiology;component/usercontrols/feedback.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\Feedback.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\..\UserControls\Feedback.xaml"
            ((KIMS.UserControls.Feedback)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.GrdMainLayout = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.MainLayout = ((System.Windows.Controls.Border)(target));
            return;
            case 4:
            this.grdFeedbackForm = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.btnCloseTeam = ((System.Windows.Controls.Button)(target));
            
            #line 162 "..\..\..\UserControls\Feedback.xaml"
            this.btnCloseTeam.PreviewTouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnBack_PreviewTouchUp);
            
            #line default
            #line hidden
            return;
            case 6:
            this.GridForm = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.svFeedback = ((System.Windows.Controls.ScrollViewer)(target));
            
            #line 183 "..\..\..\UserControls\Feedback.xaml"
            this.svFeedback.ManipulationBoundaryFeedback += new System.EventHandler<System.Windows.Input.ManipulationBoundaryFeedbackEventArgs>(this.scrollFeedBack_ManipulationBoundaryFeedback);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 213 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 214 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 10:
            
            #line 215 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 216 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 217 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 13:
            
            #line 218 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 14:
            
            #line 219 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 15:
            
            #line 220 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 16:
            
            #line 221 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 17:
            
            #line 222 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 18:
            this.txtReason01 = ((System.Windows.Controls.TextBox)(target));
            
            #line 229 "..\..\..\UserControls\Feedback.xaml"
            this.txtReason01.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchDown);
            
            #line default
            #line hidden
            
            #line 229 "..\..\..\UserControls\Feedback.xaml"
            this.txtReason01.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchLeave);
            
            #line default
            #line hidden
            
            #line 229 "..\..\..\UserControls\Feedback.xaml"
            this.txtReason01.LostFocus += new System.Windows.RoutedEventHandler(this.TextBox_LostFocus);
            
            #line default
            #line hidden
            return;
            case 19:
            
            #line 234 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 20:
            
            #line 235 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 21:
            
            #line 236 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 22:
            
            #line 237 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 23:
            
            #line 238 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 24:
            
            #line 239 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 25:
            
            #line 240 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 26:
            
            #line 241 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 27:
            
            #line 242 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 28:
            
            #line 243 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.feedbackRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 29:
            this.txtReason02 = ((System.Windows.Controls.TextBox)(target));
            
            #line 250 "..\..\..\UserControls\Feedback.xaml"
            this.txtReason02.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchDown);
            
            #line default
            #line hidden
            
            #line 250 "..\..\..\UserControls\Feedback.xaml"
            this.txtReason02.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchLeave);
            
            #line default
            #line hidden
            
            #line 250 "..\..\..\UserControls\Feedback.xaml"
            this.txtReason02.LostFocus += new System.Windows.RoutedEventHandler(this.TextBox_LostFocus);
            
            #line default
            #line hidden
            return;
            case 30:
            this.txtServiceQuest03 = ((System.Windows.Controls.TextBox)(target));
            
            #line 256 "..\..\..\UserControls\Feedback.xaml"
            this.txtServiceQuest03.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchDown);
            
            #line default
            #line hidden
            
            #line 256 "..\..\..\UserControls\Feedback.xaml"
            this.txtServiceQuest03.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchLeave);
            
            #line default
            #line hidden
            
            #line 256 "..\..\..\UserControls\Feedback.xaml"
            this.txtServiceQuest03.LostFocus += new System.Windows.RoutedEventHandler(this.TextBox_LostFocus);
            
            #line default
            #line hidden
            return;
            case 31:
            this.txtUHIDNo = ((System.Windows.Controls.TextBox)(target));
            
            #line 262 "..\..\..\UserControls\Feedback.xaml"
            this.txtUHIDNo.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchDown);
            
            #line default
            #line hidden
            
            #line 262 "..\..\..\UserControls\Feedback.xaml"
            this.txtUHIDNo.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchLeave);
            
            #line default
            #line hidden
            return;
            case 32:
            this.txtName = ((System.Windows.Controls.TextBox)(target));
            
            #line 265 "..\..\..\UserControls\Feedback.xaml"
            this.txtName.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchDown);
            
            #line default
            #line hidden
            
            #line 265 "..\..\..\UserControls\Feedback.xaml"
            this.txtName.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchLeave);
            
            #line default
            #line hidden
            return;
            case 33:
            this.dtCheckupDate = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 34:
            this.txtMobileNo = ((System.Windows.Controls.TextBox)(target));
            
            #line 272 "..\..\..\UserControls\Feedback.xaml"
            this.txtMobileNo.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchDown);
            
            #line default
            #line hidden
            
            #line 272 "..\..\..\UserControls\Feedback.xaml"
            this.txtMobileNo.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchLeave);
            
            #line default
            #line hidden
            return;
            case 35:
            this.dtPcker04 = ((System.Windows.Controls.DatePicker)(target));
            
            #line 283 "..\..\..\UserControls\Feedback.xaml"
            this.dtPcker04.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dtPckr_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 36:
            
            #line 287 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.referredByRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 37:
            
            #line 288 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.referredByRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 38:
            
            #line 289 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.referredByRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 39:
            
            #line 290 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.referredByRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 40:
            this.txtReason05 = ((System.Windows.Controls.TextBox)(target));
            
            #line 293 "..\..\..\UserControls\Feedback.xaml"
            this.txtReason05.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchDown);
            
            #line default
            #line hidden
            
            #line 293 "..\..\..\UserControls\Feedback.xaml"
            this.txtReason05.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchLeave);
            
            #line default
            #line hidden
            
            #line 293 "..\..\..\UserControls\Feedback.xaml"
            this.txtReason05.LostFocus += new System.Windows.RoutedEventHandler(this.TextBox_LostFocus);
            
            #line default
            #line hidden
            return;
            case 41:
            
            #line 299 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.referredByRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 42:
            
            #line 300 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.RadioButton)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.referredByRadioButton_TouchDown);
            
            #line default
            #line hidden
            return;
            case 43:
            this.txtEMailAddress = ((System.Windows.Controls.TextBox)(target));
            
            #line 304 "..\..\..\UserControls\Feedback.xaml"
            this.txtEMailAddress.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchDown);
            
            #line default
            #line hidden
            
            #line 304 "..\..\..\UserControls\Feedback.xaml"
            this.txtEMailAddress.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TextBox_TouchLeave);
            
            #line default
            #line hidden
            return;
            case 44:
            
            #line 307 "..\..\..\UserControls\Feedback.xaml"
            ((System.Windows.Controls.Button)(target)).TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnSubmit_TouchDown);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

