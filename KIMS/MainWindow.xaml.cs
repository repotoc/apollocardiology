﻿using ApolloDB.Models;
using KIMS.UserControls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using KIMS.AdminUserControls;
using NLog;
using KIMS.UserControls.AlertControls;
using KIMS.UserControls.DoctorControls;

namespace KIMS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        private DispatcherTimer idleTimer, autoSyncTimer;
        private int idleTimerSeconds = 0;
        private Configuration config;
        public static int zIndexValue = 9999;
        public static int ivRenTrsf;
        ManipulationModes currentMode = ManipulationModes.All;
        AdminLogin AdmLogin;
        #endregion
        public MainWindow()
        {
            InitializeComponent();
            ivRenTrsf = 10;
        }

        #region Methods
        private static int SetZIndexValue()    //increment ZIndex value method
        {
            return zIndexValue++;
        }
        private void CheckRenderTransform()
        {
            Logger.Info("Initiated");
            int countIV = 0;
            foreach (Control ctrl in GrdUserControlPanel.Children)
            {
                if (ctrl is ImageView || ctrl is VideoView || ctrl is BroucherView)
                    countIV++;
            }
            if (ivRenTrsf > 120 || countIV == 0)
                ivRenTrsf = 20;
            else
            {
                ivRenTrsf += 20;
            }
            Logger.Info("Completed");
        }
        #endregion

        #region ManipulationEventMethods
        private void HomePage_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
            {
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);
            }

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }

        private void HomePage_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            if (element != null)
            {
                MatrixTransform xform = element.RenderTransform as MatrixTransform;
                Matrix matrix = xform.Matrix;
                ManipulationDelta delta = args.DeltaManipulation;
                Point center = args.ManipulationOrigin;
                matrix.Translate(-center.X, -center.Y);
                matrix.Scale(delta.Scale.X, delta.Scale.Y);
                matrix.Translate(center.X, center.Y);
                matrix.Translate(delta.Translation.X, delta.Translation.Y);
                xform.Matrix = matrix;
            }
            args.Handled = true;
            base.OnManipulationDelta(args);
        }

        #endregion
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            idleTimerSeconds = (!(Int32.TryParse(config.AppSettings.Settings["IdleTimer"].Value, out idleTimerSeconds)) && idleTimerSeconds <= 120) ? 120 : idleTimerSeconds;
            idleTimer = new DispatcherTimer(DispatcherPriority.ApplicationIdle);
            idleTimer.Interval = new TimeSpan(0, 0, 5);
            idleTimer.Tick += idleTimer_Tick;
            InitiateAutoSync();
            LoadHomePage();
            #region Manipulation Events
            this.ManipulationStarting -= HomePage_ManipulationStarting;
            this.ManipulationDelta -= HomePage_ManipulationDelta;
            this.ManipulationStarting += HomePage_ManipulationStarting;
            this.ManipulationDelta += HomePage_ManipulationDelta;
            #endregion
            Logger.Info("Completed");
        }        

        void idleTimer_Tick(object sender, EventArgs e)
        {
            TimeSpan span = IdleTimeSetter.GetLastInput();
            if (idleTimerSeconds <= (Convert.ToInt32(span.TotalSeconds)))
            {
                GrdUserControlPanel.Children.Clear();
                LoadHomePage();
            }
        }
        private void LoadHomePage()
        {
            Logger.Info("Initiated");
            if (!(Int32.TryParse(config.AppSettings.Settings["IdleTimer"].Value, out idleTimerSeconds)) && idleTimerSeconds <= 120)
            {
                idleTimerSeconds = 120;
            }

            zIndexValue = 999;
            SlideShowUC slideshow = new SlideShowUC();
            GrdSlideshowPanel.Children.Add(slideshow);
            slideshow.EventCloseSlideShow += slideshow_EventCloseSlideShow;
            slideshow.EventOpenAdminSettings += slideshow_EventOpenAdminSettings;
            idleTimer.Stop();
            Logger.Info("Completed");
        }

        void slideshow_EventOpenAdminSettings(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            LoadAdminLogin();
            Logger.Info("Completed");
        }        

        void slideshow_EventCloseSlideShow(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            SlideShowUC slideshow = (SlideShowUC)sender;
            GrdSlideshowPanel.Children.Remove(slideshow);
            idleTimer.Start();
            LoadMenuControl();
            Logger.Info("Completed");         
        }

        #region MenuControlEvents
        private void LoadMenuControl()
        {
            Logger.Info("Initiated");
            GrdUserControlPanel.Children.Clear();
            MenuControl menu = new MenuControl();
            GrdUserControlPanel.Children.Add(menu);
            menu.EventOpenMenuItem += menu_EventOpenMenuItem;
            Logger.Info("Completed");
        }

        void menu_EventOpenMenuItem(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            Category category = sender as Category;
            if (category != null && category.IsActive.Equals(true))
            {
                if (category.LstSubCategories != null && category.LstSubCategories.Count > 0)
                {
                    //Load category page
                    if (ValidateCategoryExists(category))
                    {
                        CategoryUC categUC = new CategoryUC();
                        categUC.categoryItem = category;
                        categUC.BringIntoView();
                        Panel.SetZIndex(categUC, SetZIndexValue());
                        GrdUserControlPanel.Children.Add(categUC);
                        categUC.EventCloseCategoryUC += categUC_EventCloseCategoryUC;
                        categUC.EventOpenSubCategItem += categUC_EventOpenSubCategItem;
                    }
                }
                else if (category.LstDocuments != null && category.LstDocuments.Count > 0)
                {
                    //load document page
                    List<Document> lstDocument = category.LstDocuments.Where(b => b.IsActive).ToList();
                    LoadDocument(lstDocument, category.Name);
                }
                else if(!string.IsNullOrEmpty(category.Name))
                {
                    switch (category.Name)
                    {
                        case "Floor Plan":
                            LoadFloorPlan();
                            break;
                        case "Visiting Guidelines":
                            LoadVisitingHours();
                            break;
                        default:
                            LoadUnderDevelopmentUC();
                            break;
                    }
                }
            }
            Logger.Info("Completed");
        }

        #region VisitingHoursRegion
        private void LoadVisitingHours()
        {
            Logger.Info("Initiated");
            VisitingAndGuidelines visit = new VisitingAndGuidelines();
            GrdUserControlPanel.Children.Add(visit);
            visit.EventCloseVisitingHoursAndGuidelines += visit_EventCloseVisitingHoursAndGuidelines;
            Logger.Info("Completed");
        }

        void visit_EventCloseVisitingHoursAndGuidelines(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            VisitingAndGuidelines visit = (VisitingAndGuidelines)sender;
            GrdUserControlPanel.Children.Remove(visit);
            Logger.Info("Completed");
        } 
        #endregion

        #region FloorPlanRegion
        private void LoadFloorPlan()
        {
            Logger.Info("Initiated");
            FloorIndex floorPlan = new FloorIndex();
            GrdUserControlPanel.Children.Add(floorPlan);
            floorPlan.EventCloseFloorIndex += floorPlan_EventCloseFloorIndex;
            Logger.Info("Completed");
        }

        void floorPlan_EventCloseFloorIndex(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            FloorIndex floorPlan = (FloorIndex)sender;
            GrdUserControlPanel.Children.Remove(floorPlan);
            Logger.Info("Completed");
        } 
        #endregion

        void categUC_EventOpenSubCategItem(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            Button btnSubCategItem = sender as Button;

            SubCategory subCategItem = btnSubCategItem.DataContext as SubCategory;
            if (subCategItem != null)
            {
                if (subCategItem.LstDocuments != null && subCategItem.LstDocuments.Count > 0)
                {
                    List<Document> lstDocument = subCategItem.LstDocuments.Where(c => c.IsActive).ToList();
                    if (lstDocument != null && lstDocument.Count > 0)
                    {
                        LoadDocument(lstDocument, subCategItem.Name);
                    }
                }
                else
                {
                    LoadUnderDevelopmentUC();
                }
            }
            Logger.Info("Completed");
        }
        private void LoadDocument(List<Document> lstDocument, string subCategName)
        {
            Logger.Info("Initiated");

            CloseAllImageView();
            string docType = string.Empty;
            if (lstDocument != null && lstDocument.Count == 1)
            {
                lstDocument.Where(b => b.IsActive).ToList().ForEach(c =>
                {
                    docType = LoadByDocumentType(c);
                });
            }
            else if (lstDocument.Where(b => b.IsActive).Any(c => c.DocumentType.Equals("PPT") || c.DocumentType.Equals("PDF")))
            {
                LoadDocumentUC(lstDocument, subCategName);
            }
            else if (lstDocument.Count > 1 && lstDocument.Where(b => b.IsActive).Any(c => c.DocumentType.Equals("Video")))
            {
                LoadDocumentUC(lstDocument, subCategName);
            }
            else
            {
                lstDocument.Where(b => b.IsActive).ToList().ForEach(c =>
                {
                    docType = LoadByDocumentType(c);
                });
            }
            Logger.Info("Completed");
        }

        private string LoadByDocumentType(Document document)
        {
            Logger.Info("Initiated");
            string docType;
            docType = document.DocumentType;
            switch (docType)
            {
                case "Image":
                    LoadImageControl(document);
                    break;
                case "Video":
                    LoadVideoControl(document);
                    break;
                case "PPT":
                case "PDF":
                    LoadPresentationControl(document);
                    break;
                default:
                    break;
            }
            Logger.Info("Completed");
            return docType;
        }

        private void LoadDocumentUC(List<Document> lstDocument, string subCategName)
        {
            Logger.Info("Initiated");
            bool isExists = false;
            foreach (UIElement item in GrdUserControlPanel.Children)   //check weather DocumentsUC already exists
            {
                if (item is DocumentsUC)
                {
                    Logger.Info("Completed DocumentsUC already exists");
                    isExists = true;
                }
            }
            if(!isExists)
            {
                lstDocument = lstDocument.Where(c => c.LstDocDetails != null && c.LstDocDetails.Count > 0 && c.IsActive).ToList();
                if (lstDocument != null && lstDocument.Count > 0)
                {
                    DocumentsUC docUC = new DocumentsUC();
                    docUC.ICDocuments.ItemsSource = lstDocument;
                    docUC.tbCategoryName.Text = subCategName;
                    docUC.BringIntoView();
                    Panel.SetZIndex(docUC, SetZIndexValue());
                    GrdUserControlPanel.Children.Add(docUC);
                    docUC.EventOpenDocumentItem += docUC_EventOpenDocumentItem;
                    docUC.EventCloseDocumentUC += docUC_EventCloseDocumentUC;
                }
                else
                {
                    LoadUnderDevelopmentUC();
                }
            }
            Logger.Info("Completed");
        }

        void docUC_EventCloseDocumentUC(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GrdUserControlPanel.Children)
            {
                if (item is DocumentsUC || item is ImageView ||
                    item is VideoView || item is BroucherView)
                {
                    lstIndexes.Add(GrdUserControlPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GrdUserControlPanel.Children.RemoveAt(c);   //remove usercontrols by its index values
            });
            Logger.Info("Completed");
        }

        void docUC_EventOpenDocumentItem(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            Button btn = sender as Button;
            Document selectedDocument = btn.DataContext as Document;
            if (selectedDocument != null)
            {
                LoadByDocumentType(selectedDocument);
            }
            Logger.Info("Completed");
        }
        private bool ValidateCategoryExists(Category category)
        {
            Logger.Info("Initiated");
            foreach (Control item in GrdUserControlPanel.Children)
            {
                if (item is CategoryUC)
                {
                    item.BringIntoView();
                    Panel.SetZIndex(item, SetZIndexValue());
                    Logger.Info("Completed CategoryUC already exists");
                    return false;
                }
            }
            Logger.Info("Completed");
            return true;
        }
        void categUC_EventCloseCategoryUC(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GrdUserControlPanel.Children)
            {
                if (item is CategoryUC || item is ImageView || item is VideoView || item is BroucherView)
                {
                    lstIndexes.Add(GrdUserControlPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GrdUserControlPanel.Children.RemoveAt(c);   //remove usercontrols by its index values
            });
            Logger.Info("Completed");
        }

        #endregion

        #region LoadBroucherControlMethod
        private void LoadPresentationControl(Document selectedDocument)
        {
            Logger.Info("Initiated");
            if (CheckBroucherExists(selectedDocument))
            {
                //Load PresentationControl usercontrol
                if (selectedDocument != null && selectedDocument.LstDocDetails != null && selectedDocument.LstDocDetails.Count > 0)
                {
                    CheckRenderTransform();
                    BroucherView broucher = new BroucherView();
                    broucher.IsManipulationEnabled = true;
                    broucher.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
                    broucher.BringIntoView();
                    Panel.SetZIndex(broucher, SetZIndexValue());
                    broucher.listCategoryData = selectedDocument.LstDocDetails.Where(c => c.IsActive).ToList();
                    GrdUserControlPanel.Children.Add(broucher);
                    broucher.EventCloseBroucher -= broucher_EventCloseBroucher;
                    broucher.EventCloseBroucher += broucher_EventCloseBroucher;
                }
            }
            Logger.Info("Completed");
        }

        private bool CheckBroucherExists(Document docItem)
        {
            Logger.Info("Initiated");
            int count = 0;
            foreach (Control item in GrdUserControlPanel.Children)
            {
                if (item is BroucherView)
                {
                    docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                    {
                        BroucherView broucher = item as BroucherView;
                        broucher.listCategoryData.ForEach(d =>
                        {
                            if (d.ID.Equals(c.ID))
                            {
                                item.BringIntoView();
                                Panel.SetZIndex(item, SetZIndexValue());
                                count++;
                                Logger.Info("Completed BroucherView already exists");
                            }
                        });
                    });
                }
            }
            if (count > 0)
            {
                Logger.Info("Completed BroucherView already exists");
                return false;
            }
            else
            {
                Logger.Info("Completed");
                return true;
            }
        }
        void broucher_EventCloseBroucher(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            BroucherView broucher = (BroucherView)sender;
            GrdUserControlPanel.Children.Remove(broucher);
            Logger.Info("Completed");
        }
        #endregion

        #region LoadImageControlMethod
        private void LoadImageControl(Document docItem)
        {
            Logger.Info("Initiated");
            if (CheckImageExists(docItem))
            {
                docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                {
                    CheckRenderTransform();
                    ImageView imgView = new ImageView();
                    imgView.IsManipulationEnabled = true;
                    imgView.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
                    imgView.BringIntoView();
                    imgView.documentDets = c;
                    Panel.SetZIndex(imgView, SetZIndexValue());
                    imgView.DataContext = c;
                    //imgView.img.Source = new BitmapImage(new Uri(c.FileLocation));
                    GrdUserControlPanel.Children.Add(imgView);
                    //raise events from ImageView
                    imgView.EventCloseImageView += imgView_EventCloseImageView;
                });
            }
            Logger.Info("Completed");
        }

        private void CloseAllImageView()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GrdUserControlPanel.Children)
            {
                if (item is ImageView)
                {
                    lstIndexes.Add(GrdUserControlPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GrdUserControlPanel.Children.RemoveAt(c);   //remove usercontrols by its index values
            });
            Logger.Info("Completed");
        }

        private bool CheckImageExists(Document docItem)
        {
            Logger.Info("Initiated");
            int count = 0;
            foreach (Control item in GrdUserControlPanel.Children)
            {
                if (item is ImageView)
                {
                    docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                    {
                        DocumentDetails doc = item.DataContext as DocumentDetails;
                        if (doc.ID.Equals(c.ID))
                        {
                            item.BringIntoView();
                            Panel.SetZIndex(item, SetZIndexValue());
                            count++;
                        }
                    });
                }
            }
            if (count > 0)
            {
                Logger.Info("Completed DocumentDetails already exists");
                return false;
            }
            else
            {
                Logger.Info("Completed");
                return true;
            }
        }

        void imgView_EventCloseImageView(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            //remove ImageView fro GrdUserControlPanel
            ImageView imgView = (ImageView)sender;
            GrdUserControlPanel.Children.Remove(imgView);
            Logger.Info("Completed");
        }
        #endregion

        #region LoadVideoControlMethod
        private void LoadVideoControl(Document docItem)
        {
            Logger.Info("Initiated");
            //Load video usercontrol here
            if (CheckVideoExists(docItem))
            {
                docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                {
                    CheckRenderTransform();
                    VideoView vdoView = new VideoView();
                    vdoView.IsManipulationEnabled = true;
                    vdoView.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
                    vdoView.BringIntoView();
                    Panel.SetZIndex(vdoView, SetZIndexValue());
                    vdoView.documentDets = c;
                    GrdUserControlPanel.Children.Add(vdoView);
                    //raise events from VideoView
                    vdoView.EventCloseVideoView += vdoView_EventCloseVideoView;
                });
            }
            Logger.Info("Completed");
        }

        private bool CheckVideoExists(Document docItem)
        {
            Logger.Info("Initiated");
            int count = 0;
            foreach (Control item in GrdUserControlPanel.Children)
            {
                if (item is VideoView)
                {
                    docItem.LstDocDetails.Where(b => b.IsActive).ToList().ForEach(c =>
                    {
                        DocumentDetails doc = item.DataContext as DocumentDetails;
                        if (doc != null)
                        {
                            if (doc.ID.Equals(c.ID))
                            {
                                item.BringIntoView();
                                Panel.SetZIndex(item, SetZIndexValue());
                                count++;
                            }
                        }
                    });
                }
            }
            if (count > 0)
            {
                Logger.Info("Completed DocumentDetails already exists");
                return false;
            }
            else
            {
                Logger.Info("Completed");
                return true;
            }
        }

        void vdoView_EventCloseVideoView(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            //remove VideoView from GrdUserControlPanel
            VideoView vdoView = (VideoView)sender;
            GrdUserControlPanel.Children.Remove(vdoView);
            Logger.Info("Completed");
        }
        #endregion
                

        public static int SetAxisValue(int axisValue)
        {
            axisValue = axisValue + 10;
            return axisValue;
        }
        
        #region AlertControlMethods

        private void LoadUnderDevelopmentUC()
        {
            Logger.Info("Initiated");
            UnderDevelopmentUC dev = new UnderDevelopmentUC();
            GrdAlertControlPanel.Children.Add(dev);
            dev.EventCloseDevelopmentUC += dev_EventCloseDevelopmentUC;
            Logger.Info("Completed");
        }
        void dev_EventCloseDevelopmentUC(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            UnderDevelopmentUC dev = (UnderDevelopmentUC)sender;
            GrdAlertControlPanel.Children.Remove(dev);
            Logger.Info("Completed");
        }
        private void WarningAlert(string msg)
        {
            Logger.Info("Initiated");
            WarningAlertUC wAlert = new WarningAlertUC();
            wAlert.tblSuccessMessage.Text = msg;
            GrdAlertControlPanel.Children.Add(wAlert);
            wAlert.EventCloseWarningAlert += wAlert_EventCloseWarningAlert;
            Logger.Info("Completed");
        }

        void wAlert_EventCloseWarningAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            WarningAlertUC wAlert = (WarningAlertUC)sender;
            GrdAlertControlPanel.Children.Remove(wAlert);
            Logger.Info("Completed");
        }

        private void ErrorAlert(string msg)
        {
            Logger.Info("Initiated");
            ErrorAlertUC eAlert = new ErrorAlertUC();
            eAlert.tblSuccessMessage.Text = msg;
            GrdAlertControlPanel.Children.Add(eAlert);
            eAlert.EventCloseErrorAlert += eAlert_EventCloseErrorAlert;
            Logger.Info("Completed");
        }

        void eAlert_EventCloseErrorAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ErrorAlertUC eAlert = (ErrorAlertUC)sender;
            GrdAlertControlPanel.Children.Remove(eAlert);
            Logger.Info("Completed");
        }

        private void SuccessAlert(string msg)
        {
            Logger.Info("Initiated");
            SuccessAlertUC sAlert = new SuccessAlertUC();
            sAlert.tblSuccessMessage.Text = msg;
            GrdAlertControlPanel.Children.Add(sAlert);
            sAlert.EventCloseSuccessAlert += sAlert_EventCloseSuccessAlert;
            Logger.Info("Completed");
        }

        void sAlert_EventCloseSuccessAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            SuccessAlertUC sAlert = (SuccessAlertUC)sender;
            GrdAlertControlPanel.Children.Remove(sAlert);
            Logger.Info("Completed");
        }
                
        #endregion

        #region AutoSyncRegion
        private void InitiateAutoSync()
        {
            Logger.Info("Initiated");
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                if (Convert.ToBoolean(config.AppSettings.Settings["AutoSyncEnabled"].Value))
                {
                    DateTime autoSyncTime = DateTime.Now;

                    if (DateTime.TryParse(config.AppSettings.Settings["AutoSyncTiming"].Value, out autoSyncTime))
                    {
                        string syncTime = autoSyncTime.ToString("HH:mm");
                        if (autoSyncTimer != null)
                        {
                            autoSyncTimer.Stop();
                            autoSyncTimer = null;
                        }
                        DateTime fromDate = DateTime.Now;
                        DateTime fetchSyncDate = Convert.ToDateTime(DateTime.Today.ToShortDateString() + " " + syncTime);

                        TimeSpan tspan = fetchSyncDate - fromDate;

                        if (tspan.Seconds < 0)
                        {
                            fetchSyncDate = Convert.ToDateTime(DateTime.Today.AddDays(1).ToShortDateString() + " " + syncTime);
                            tspan = fetchSyncDate - fromDate;
                        }
                        if (autoSyncTimer == null)
                        {
                            autoSyncTimer = new DispatcherTimer();
                            autoSyncTimer.Interval = new TimeSpan(Convert.ToInt32(tspan.Hours), Convert.ToInt32(tspan.Minutes), Convert.ToInt32(tspan.Seconds));
                            autoSyncTimer.Tick += autoSyncTimer_Tick;
                            autoSyncTimer.Start();
                        }
                    }
                }
                Logger.Info("Completed");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
        void autoSyncTimer_Tick(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            if (!CheckAutosyncExists())
            {
                AutoSyncUC sync = new AutoSyncUC();
                GrdSyncControlPanel.Children.Add(sync);
                sync.EventCloseSetAutoSync += sync_EventCloseSetAutoSync;
                
            }
            autoSyncTimer.Stop();
            Logger.Info("Completed");
        }
        void sync_EventCloseSetAutoSync(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            //remove autosyncuc from grdsynccontrolpanel after sync
            AutoSyncUC sync = (AutoSyncUC)sender;
            GrdSyncControlPanel.Children.Remove(sync);
            Logger.Info("Completed");
        }
        private bool CheckAutosyncExists()  //method to check autosyncuc already exists or not
        {
            Logger.Info("Initiated");
            foreach (UIElement item in GrdSyncControlPanel.Children)
            {
                if (item is AutoSyncUC)
                {
                    Logger.Info("Completed Returns True");
                    return true;
                }
            }
            Logger.Info("Completed Returns False");
            return false;
        }
        
        #endregion
        private void LoadAdminLogin()
        {
            bool isExists = false;
            foreach (UIElement item in GrdAdminPanel.Children)
            {
                if (item is AdminLogin)
                {
                    isExists = true;
                }
            }
            if (!isExists)
            {
                Logger.Info("Initiated");
                AdmLogin = new AdminLogin();
                GrdAdminPanel.Children.Clear();
                GrdAdminPanel.Children.Add(AdmLogin);
                AdmLogin.EventWarningAlert += AdmLogin_EventWarningAlert;
                AdmLogin.EventCloseAdminSetting += AdmLogin_EventCloseAdminSetting;
                AdmLogin.EventLoginAdmin += AdmLogin_EventLoginAdmin;
                Logger.Info("Completed");
            }
        }

        void AdmLogin_EventWarningAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = (sender is string) ? (string)sender : string.Empty;
            WarningAlert(msg);
            Logger.Info("Completed");
        }
        void AdmLogin_EventCloseAdminSetting(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            GrdAdminPanel.Children.Clear();
            Logger.Info("Completed");
        }
        void AdmLogin_EventLoginAdmin(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            LoadAdminSettings();
            Logger.Info("Completed");
        }
        AdminSettings AdmSetting;
        private void LoadAdminSettings()
        {
            bool isExists = false;
            foreach (UIElement item in GrdAdminPanel.Children)
            {
                if (item is AdminSettings)
                {
                    isExists = true;
                }
            }
            if (!isExists)
            {
                Logger.Info("Initiated");
                AdmSetting = new AdminSettings();
                GrdAdminPanel.Children.Clear();
                GrdAdminPanel.Children.Add(AdmSetting);
                AdmSetting.EventLogoutSettings += AdmSetting_EventLogoutSettings;
                AdmSetting.EventQuestions += AdmSetting_EventQuestions;
                AdmSetting.EventEmailConfig += AdmSetting_EventEmailConfig;
                AdmSetting.EventSlideShowIdleTimer += AdmSetting_EventSlideShowIdleTimer;
                AdmSetting.EventReports += AdmSetting_EventReports;
                AdmSetting.EventOpenSetAutoSync += AdmSetting_EventOpenSetAutoSync;
                Logger.Info("Completed");
            }
        }

        void AdmSetting_EventEmailConfig(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            GrdAdminPanel.Children.Clear();
            EmailConfigurationUC emailConfig = new EmailConfigurationUC();
            GrdAdminPanel.Children.Add(emailConfig);
            emailConfig.EvntCloseEmailSetting += emailConfig_EvntCloseEmailSetting;
            emailConfig.EvntSuccessAlert += emailConfig_EvntSuccessAlert;
            emailConfig.EvntWarningAlert += emailConfig_EvntWarningAlert;
            emailConfig.EvntErrorAlert += emailConfig_EvntErrorAlert;
            Logger.Info("Completed");
        }

        void emailConfig_EvntErrorAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = (sender is string) ? (string)sender : string.Empty;
            ErrorAlert(msg);
            Logger.Info("Initiated");
        }

        void emailConfig_EvntWarningAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = (sender is string) ? (string)sender : string.Empty;
            WarningAlert(msg);
            Logger.Info("Initiated");
        }

        void emailConfig_EvntSuccessAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = (sender is string) ? (string)sender : string.Empty;
            SuccessAlert(msg);
            Logger.Info("Initiated");
        }

        void emailConfig_EvntCloseEmailSetting(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            EmailConfigurationUC emailConfig = (EmailConfigurationUC)sender;
            GrdAdminPanel.Children.Remove(emailConfig);
            LoadAdminSettings();
            Logger.Info("Completed");            
        }

        void AdmSetting_EventOpenSetAutoSync(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            GrdAdminPanel.Children.Clear();
            SetAutoSyncTime autosync = new SetAutoSyncTime();
            GrdAdminPanel.Children.Add(autosync);
            autosync.autoSyncRefresh += autosync_autoSyncRefresh;
            autosync.EventSyncNow += autosync_EventSyncNow;
            autosync.EventSuccessAlert += autosync_EventSuccessAlert;
            autosync.EventErrorAlert += autosync_EventErrorAlert;
            autosync.EventCloseSetAutoSync += autosync_EventCloseSetAutoSync;
            Logger.Info("Completed");
        }

        void autosync_EventSyncNow(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            InitiateSync();
            Logger.Info("Completed");            
        }

        private void InitiateSync()
        {
            Logger.Info("Initiated");
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);

                DateTime autoSyncTime = DateTime.Now.AddMilliseconds(10);

                string syncTime = autoSyncTime.ToString("HH:mm");
                if (autoSyncTimer != null)
                {
                    autoSyncTimer.Stop();
                    autoSyncTimer = null;
                }
                DateTime fromDate = DateTime.Now;
                DateTime fetchSyncDate = Convert.ToDateTime(DateTime.Today.ToShortDateString() + " " + syncTime);

                TimeSpan tspan = fetchSyncDate - fromDate;

                if (tspan.Seconds < 0)
                {
                    fetchSyncDate = Convert.ToDateTime(DateTime.Today.AddDays(1).ToShortDateString() + " " + syncTime);
                    tspan = new TimeSpan(0, 0, 1); // fetchSyncDate - fromDate;
                }
                if (autoSyncTimer == null)
                {
                    autoSyncTimer = new DispatcherTimer();
                    autoSyncTimer.Interval = new TimeSpan(Convert.ToInt32(tspan.Hours), Convert.ToInt32(tspan.Minutes), Convert.ToInt32(tspan.Seconds));
                    autoSyncTimer.Tick += autoSyncTimer_Tick;
                    autoSyncTimer.Start();
                }
                Logger.Info("Completed");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        void autosync_autoSyncRefresh(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            InitiateAutoSync();
            Logger.Info("Completed");
        }
        void autosync_EventSuccessAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string infoMsg = (sender is string) ? (string)sender : string.Empty;
            SuccessAlert(infoMsg);
            Logger.Info("Completed");
        }
        void autosync_EventErrorAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string infoMsg = (sender is string) ? (string)sender : string.Empty;
            ErrorAlert(infoMsg);
            Logger.Info("Completed");
        }
        void autosync_EventCloseSetAutoSync(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            SetAutoSyncTime autosync = (SetAutoSyncTime)sender;
            GrdAdminPanel.Children.Remove(autosync);
            LoadAdminSettings();
            Logger.Info("Completed");
        }
        void AdmSetting_EventReports(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ReportsUC report = new ReportsUC();
            GrdAdminPanel.Children.Clear();
            GrdAdminPanel.Children.Add(report);
            report.EventCloseReportUC += report_EventCloseReportUC;
            Logger.Info("Completed");
        }

        void report_EventCloseReportUC(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            GrdAdminPanel.Children.Clear();
            LoadAdminSettings();
            Logger.Info("Completed");
        }

        void AdmSetting_EventSlideShowIdleTimer(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            GrdAdminPanel.Children.Clear();
            SlideShowIdleTimer ssIdleTimer = new SlideShowIdleTimer();
            GrdAdminPanel.Children.Add(ssIdleTimer);
            //Raise events from SlideShowIdleTimer control
            ssIdleTimer.EventCloseIdleTimer += ssIdleTimer_EventCloseIdleTimer;
            ssIdleTimer.EventSuccessAlert += ssIdleTimer_EventSuccessAlert;
            ssIdleTimer.EventWarningAlert += ssIdleTimer_EventWarningAlert;
            Logger.Info("Completed");
        }

        void ssIdleTimer_EventWarningAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = (sender is string) ? (string)sender : string.Empty;
            WarningAlert(msg);
            Logger.Info("Completed");
        }

        void ssIdleTimer_EventSuccessAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = (sender is string) ? (string)sender : string.Empty;
            SuccessAlert(msg);
            Logger.Info("Completed");
        }

        void ssIdleTimer_EventCloseIdleTimer(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            SlideShowIdleTimer ssIdleTimer = new SlideShowIdleTimer();
            GrdAdminPanel.Children.Remove(ssIdleTimer);
            LoadAdminSettings();
            Logger.Info("Completed");
        }

        private void AdmSetting_EventQuestions(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            QuestionsUC ques = new QuestionsUC();
            GrdAdminPanel.Children.Clear();
            GrdAdminPanel.Children.Add(ques);
            ques.EventCloseQuestionUC += ques_EventCloseQuestionUC;
            Logger.Info("Completed");
        }

        void ques_EventCloseQuestionUC(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            GrdAdminPanel.Children.Clear();
            LoadAdminSettings();
            Logger.Info("Completed");
        }
        void AdmSetting_EventLogoutSettings(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            GrdAdminPanel.Children.Clear();
            Logger.Info("Completed");
        }
               
        private void HomeLogo_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            GrdAdminPanel.Children.Clear();
            GrdUserControlPanel.Children.Clear();
            GrdHeaderUCPanel.Children.Clear();
            LoadHomePage();
            Logger.Info("Completed");
        }        

        private void btnContactUs_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            bool isExists = false;
            foreach (UIElement item in GrdHeaderUCPanel.Children)
            {
                if (item is ContactUC)
                {
                    Panel.SetZIndex(item, SetZIndexValue());
                    isExists = true;
                }
            }
            if (!isExists)
            {
                Logger.Info("Initiated");
                ContactUC contact = new ContactUC();
                contact.RenderTransform = new MatrixTransform(1, 0, 0, 1, 1, 1);
                contact.BringIntoView();
                Panel.SetZIndex(contact, SetZIndexValue());
                GrdHeaderUCPanel.Children.Add(contact);
                contact.EventCloseContactUC += contact_EventCloseContactUC;
                Logger.Info("Completed");
                //raise events of team uc
            }
        }

        void contact_EventCloseContactUC(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ContactUC contact = (ContactUC)sender;
            GrdHeaderUCPanel.Children.Remove(contact);
            Logger.Info("Completed");
        }

        #region SpecialityRegion
        private void btnSpeciality_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            bool isExists = false;
            foreach (UIElement item in GrdHeaderUCPanel.Children)
            {
                if (item is DepartmentUC)
                {
                    Panel.SetZIndex(item, SetZIndexValue());
                    isExists = true;
                }
            }
            if (!isExists)
            {
                Logger.Info("Initiated");
                DepartmentUC dept = new DepartmentUC();
                dept.BringIntoView();
                Panel.SetZIndex(dept, SetZIndexValue());
                GrdHeaderUCPanel.Children.Add(dept);
                //raise events of team uc
                dept.EventOpenDoctorProfile += dept_EventOpenDoctorProfile;
                dept.EventCloseDepartment += dept_EventCloseDepartment;
                Logger.Info("Completed");
            }
        }

        void dept_EventOpenDoctorProfile(object sender, EventArgs e)
        {
            Doctor doctor = sender as Doctor;
            if (CheckDocProfileExists(doctor))
            {
                Logger.Info("Initiated");
                CheckRenderTransform();
                DoctorProfileView docProfile = new DoctorProfileView();
                docProfile.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsf, ivRenTrsf);
                docProfile.BringIntoView();
                docProfile.docItem = doctor;
                Panel.SetZIndex(docProfile, SetZIndexValue());
                GrdHeaderUCPanel.Children.Add(docProfile);
                docProfile.EventCloseDocView += docProfile_EventCloseDocView;
                Logger.Info("Completed");
            }
        }

        private bool CheckDocProfileExists(Doctor doc)
        {
            foreach (UIElement  item in GrdHeaderUCPanel.Children)
            {
                if(item is DoctorProfileView)
                {
                    DoctorProfileView docItem = item as DoctorProfileView;
                    if (docItem.DataContext.Equals(doc))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        void docProfile_EventCloseDocView(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            DoctorProfileView docProfile = (DoctorProfileView)sender;
            GrdHeaderUCPanel.Children.Remove(docProfile);
            Logger.Info("Completed");
        }

        void dept_EventCloseDepartment(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GrdHeaderUCPanel.Children)
            {
                if (item is DepartmentUC || item is DoctorProfileView)
                {
                    lstIndexes.Add(GrdHeaderUCPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GrdHeaderUCPanel.Children.RemoveAt(c);   //remove usercontrols by its index values
            });
            Logger.Info("Completed");
        }        
        #endregion

        private void btnFeedback_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Feedback feedback = new Feedback();
            feedback.RenderTransform = new MatrixTransform(1, 0, 0, 1, 1, 1);
            feedback.BringIntoView();
            Panel.SetZIndex(feedback, SetZIndexValue());
            GrdHeaderUCPanel.Children.Add(feedback);
            feedback.EventCloseFeedback += feedback_EventCloseFeedback;
            feedback.EventSuccessAlert += feedback_EventSuccessAlert;
            feedback.EventWarningAlert += feedback_EventWarningAlert;
            Logger.Info("Completed");
        }
        
        void feedback_EventWarningAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = (sender is string) ? (string)sender : string.Empty;
            WarningAlert(msg);
            Logger.Info("Completed");             
        }

        void feedback_EventSuccessAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = (sender is string) ? (string)sender : string.Empty;
            SuccessAlert(msg);
            Logger.Info("Completed");            
        }

        void feedback_EventCloseFeedback(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            Feedback feedback = (Feedback)sender;
            GrdHeaderUCPanel.Children.Remove(feedback);
            Logger.Info("Completed");            
        }
    }
}